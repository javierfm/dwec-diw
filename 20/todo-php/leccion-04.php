<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Todo PHP. Un buscador para nuestra base de datos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../css/styles.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Un buscador para nuestra base de datos</h1>
        </header> <!-- fin cabecera -->
        <section>
            <a href="index.html">Home</a> > Un buscador para nuestra base de datos
        </section> <!-- fin miga de pan -->
        <form method="POST" action="buscador.php" id="busqueda"> 
            <strong>Buscar un nombre:</strong> <input type="text" name="query" size="20"><br><br> 
            <input type="submit" value="Buscar" name="buscar"> 
        </form> <!-- fin #busqueda -->
        <footer>
            Todo PHP. 2016 2º DAW
        </footer><!-- fin pie de página -->
    </body>
</html>