jQuery.fn.cuentaCaracteres = function () {
	//this es una referencia al objeto que recibe este método, puede ser un array de objetos, luego se itera
	this.each(function () {
		//"se convierte" la referencia al objeto en un objeto jQuery para poder aplicar los métodos del framework
		elemento = $(this);
		//Elemento que muestra el número de caracteres
		var contador = $('<div>Contador de caracteres: ' + elemento.prop("value").length + '</div>');
		//inserto el elemento después del textArea
		elemento.after(contador);
		//guardo una referencia al elemento DIV en los datos del objeto jQuery
		elemento.data('campocontador', contador);
		//Se crea un evento keyup para el objeto sobre el que estamos iterando
		elemento.keyup(function () {
			//Referencia al objeto
			var elemento = $(this);
			//Recupero el DIV que se guardo como data
			var campoContador = elemento.data('campocontador');
			/* Actualizo el campo de texto, es un objeto jQuery donde hay un div, luego podemos acceder al html y sus propiedades*/
			campoContador.text('Contador de caracteres:' + elemento.prop("value").length);
		});
	});
	//Siempre hay que devolver
	return this;
}