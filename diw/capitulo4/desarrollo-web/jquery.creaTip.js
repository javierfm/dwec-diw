/* Se reciben dos parámetros: El texto que llevará el tip y unas opciones específicas de configuracion*/
jQuery.fn.creaTip = function (textoTip, opciones) {
	//opciones por defecto que tendrá el plugin
	var configuracion = {
			velocidad: 500
			, animacionEntrada: {
				width: 'show'
			}
			, animacionSalida: {
				opacity: 'hide'
			}
			, claseTip: 'tip'
		}
		//Se extienden las opciones por defecto con las opciones de parámetro. A las opciones por defecto se le suman las recibidas.
	jQuery.extend(configuracion, opciones);
	//this es una referencia al objeto que recibe este método, puede ser un array de objetos, luego se itera
	this.each(function () {
		//"se convierte" la referencia al objeto en un objeto jQuery para poder aplicar los métodos del framework
		elemento = $(this);
		//Creo objeto que será el div del tooltip
		var miTip = $('<div class="' + configuracion.claseTip + '">' + textoTip + '</div>');
		//Agrego el tip al documento
		$(document.body).append(miTip);
		//Guardo esta capa como dato el objeto sobre el que está la iteracion
		elemento.data('capaTip', miTip);
		//Evento para cuando el raton entre en el objeto a la que aplicamos este plugin
		elemento.mouseenter(function (evento) {
			var miTip = $(this).data('capaTip');
			console.info(miTip);
			miTip.css({
				'left': evento.pageX + 10
				, 'top': evento.pageY + 5
			});
			miTip.animate(configuracion.animacionEntrada, configuracion.velocidad);
		});
		//evento para  cuando el ratón salga del objeto
		elemento.mouseleave(function (evento) {
			var miTip = $(this).data('capaTip');
			miTip.animate(configuracion.animacionSalida, configuracion.velocidad);
		});
	});
	//Siempre hay que devolver
	return this;
}