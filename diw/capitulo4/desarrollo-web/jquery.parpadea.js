jQuery.fn.parpadea = function () {
    //this es una referencia al objeto que recibe este método
    this.each(function () {
        //"se convierte" la referencia al objeto en un objeto jQuery para poder aplicar los métodos del framework
        elemento = $(this);
        elemento.fadeOut(250, function () {
            /* No se puede usar elemento, hay que usar una referencia del objeto que lanza el callback*/
            $(this).fadeIn(300);
        });
    });
}