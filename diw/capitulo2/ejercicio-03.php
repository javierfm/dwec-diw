<!doctype html>
<html lang="es">

<head>
	<meta charset="UTF-8" />
	<title>Envío de email usando desde localhost</title>
	<!-- enlace a hoja de estilos generales -->
	<link rel="stylesheet" type="text/css" href="../../css/styles.css"> </head>

<body>
	<div id="cabecera">
		<a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
	</div>
	<div id="navegacion">
		<ul>
			<li><a href="/miWebVirtual/">Javier Fernández</a></li>
			<li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
			<li><a href="index.html">Funcionalidades para una aplicación web</a></li>
			<li class="activo">Envío de email usando desde localhost</li>
		</ul>
		<div style="clear: left"></div>
	</div>
	<div id="contenedor">
		<p>Para ver el funcionamiento correcto del ejercicio debería ejecutarse desde un servidor en local, a nivel de código es el mismo del <a href="ejercicio-02.php">ejercicio anterior</a>.</p>
		<p>La diferencia reside en que hay que modificar la configuración en <em>PHP.ini</em> para indicar que vamos a usar un servidor de correo local</p>
	</div>
</body>

</html>