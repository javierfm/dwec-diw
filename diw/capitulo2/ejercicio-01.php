<?php
require('conexion.php');
$sql = "SELECT * FROM encuestas ORDER BY id DESC";
$req = mysql_query($sql);
?>
	<!doctype html>
	<html lang="es">

	<head>
		<meta charset="UTF-8" />
		<title>Modelo de encuesta</title>
		<!-- enlace a hoja de estilos generales -->
		<link rel="stylesheet" type="text/css" href="../../css/styles.css"> </head>
	<!-- estilos propios del ejercicio -->
	<style type="text/css">
		.barra {
			background: #ff3019;
			background: -moz-linear-gradient(top, #ff3019 0%, #cf0404 100%);
			background: -webkit-linear-gradient(top, #ff3019 0%, #cf0404 100%);
			background: linear-gradient(to bottom, #ff3019 0%, #cf0404 100%);
			clear: both;
			height: 15px;
			color: white;
			font-weight: bold;
			text-align: right;
			padding: 6px;
			border-radius: 4px;
			max-width: 356px;
			min-width: 20px;
		}
	</style>

	<body>
		<div id="cabecera">
			<a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
		</div>
		<div id="navegacion">
			<ul>
				<li><a href="/miWebVirtual/">Javier Fernández</a></li>
				<li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
				<li><a href="index.html">Funcionalidades para una aplicación web</a></li>
				<li class="activo">Modelo de encuesta</li>
			</ul>
			<div style="clear: left"></div>
		</div>
		<div id="contenedor">
			<h2>Encuesta:</h2>
			<ul class="votacion index">
				<?php
			while($result = mysql_fetch_object($req)){
				echo '<li><a href="encuesta.php?id='.$result->id.'">'.$result->titulo.'</a></li>';
			}
		?>
	</body>

	</html>