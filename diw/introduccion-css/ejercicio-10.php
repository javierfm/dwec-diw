<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio 10</title>
    <!-- enlace a hoja de estilos generales Se desactivan para no interferir con los del ejercicio -->
    <!-- <link rel="stylesheet" type="text/css" href="../../css/styles.css"> -->

<style type="text/css">
ul.menu {
  width: 180px;
  list-style: none;
  margin: 0;
  padding: 0;
  border: 1px solid #7C7C7C;
}
ul.menu li {
  border-bottom: 1px solid #7C7C7C;
  border-top: 1px solid #FFF;
  background: #F4F4F4;
}
ul.menu li a {
  padding: .2em 0 .2em 1.3em;
  display: block;
  text-decoration: none;
  color: #333;
    background: #F4F4F4 url("img/flecha_inactiva.png") no-repeat 3px;
}

ul.menu li a:hover, ul.menu li a:active {
  background: #E4E4E4 url("img/flecha_activa.png") no-repeat 3px;
}
</style>
 

<body>
<div id="navegacion">
  <ul>
    <li><a href="/miWebVirtual/">Javier Fernández</a></li>
    <li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li><a href="index.php">Introducción a CSS</a></li>
    <li class="activo">Ejercicio 10</li>
  </ul>
  <div style="clear: left"></div>
</div>

<ul class="menu">
  <li><a href="#" title="Enlace gen�rico">Elemento 1</a></li>
  <li><a href="#" title="Enlace gen�rico">Elemento 2</a></li>
  <li><a href="#" title="Enlace gen�rico">Elemento 3</a></li>
  <li><a href="#" title="Enlace gen�rico">Elemento 4</a></li>
  <li><a href="#" title="Enlace gen�rico">Elemento 5</a></li>
  <li><a href="#" title="Enlace gen�rico">Elemento 6</a></li>
</ul>


</body>

</html>