<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio 9</title>
    <!-- enlace a hoja de estilos generales Se desactivan para no interferir con los del ejercicio -->
    <!-- <link rel="stylesheet" type="text/css" href="../../css/styles.css"> -->

    <style type="text/css">

    #galeria img{
      float: left;
      padding: .5em;
      margin: 1.4em;
      border:1px solid grey;
    }
    }
</style>
 

<body>
<div id="navegacion">
  <ul>
    <li><a href="/miWebVirtual/">Javier Fernández</a></li>
    <li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li><a href="index.php">Introducción a CSS</a></li>
    <li class="activo">Ejercicio 8</li>
  </ul>
  <div style="clear: left"></div>
</div>

<div id="galeria">
<img src="img/imagen.png" alt="Imagen generica"/>
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
<img src="img/imagen.png" alt="Imagen generica" />
</div>


</body>

</html>