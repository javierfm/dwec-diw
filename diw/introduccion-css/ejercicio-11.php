<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio 11</title>
    <!-- enlace a hoja de estilos generales Se desactivan para no interferir con los del ejercicio -->
    <!-- <link rel="stylesheet" type="text/css" href="../../css/styles.css"> -->

<style type="text/css">
table {
  font: .9em Arial, Helvetica, sans-serif;
  border: 1px solid #333;
  border-collapse: collapse;
  text-align: center;
}
table th {
  background: #E6F3FF url(img/fondo_gris.gif) repeat-x;
  padding: 0 .3em;
}
table th.euro {
  background: #E6F3FF url(img/euro.png) no-repeat left center;
  padding: 0 .3em 0 1.2em;
}
table th.dolar {
  background: #E6F3FF url(img/dolar.png) no-repeat left center;
  padding: 0 .3em 0 1.2em;
}
table th.libra {
  background: #E6F3FF url(img/libra.png) no-repeat left center;
  padding: 0 .3em 0 1.2em;
}
table th.yen {
  background: #E6F3FF url(img/yen.png) no-repeat left center;
  padding: 0 .3em 0 1.2em;
}
table th, table, td {
  border: 1px solid #333;
  line-height: 2em;
}
.fila {
  text-align: left;
}
.par {
  background-color:#FFFFCC;
}
table tr:hover {
  background: #FFFF66 !important;
}
</style>
</style>
 

<body>
<div id="navegacion">
  <ul>
    <li><a href="/miWebVirtual/">Javier Fernández</a></li>
    <li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li><a href="index.php">Introducción a CSS</a></li>
    <li class="activo">Ejercicio 11</li>
  </ul>
  <div style="clear: left"></div>
</div>

<table border="1" summary="Tipos de cambio">
  <tr>
    <th scope="col">Cambio</th>
    <th scope="col">Compra</th>
    <th scope="col">Venta</th>
    <th scope="col">M&aacute;ximo</th>
    <th scope="col">M&iacute;nimo</th>
  </tr>
  <tr class="par">
    <th scope="row" class="fila euro">Euro/Dolar</th>
    <td>1.2524</td>
    <td>1.2527</td>
    <td>1.2539</td>
    <td>1.2488</td>
  </tr>
  <tr>
    <th scope="row" class="fila dolar">Dolar/Yen</th>
    <td>119.01</td>
    <td>119.05</td>
    <td>119.82</td>
    <td>119.82</td>
  </tr>
  <tr class="par">
    <th scope="row" class="fila libra">Libra/Dolar</th>
    <td>1.8606</td>
    <td>1.8611</td>
    <td>1.8651</td>
    <td>1.8522</td>
  </tr>
  <tr>
    <th scope="row" class="fila yen">Euro/Yen</th>
    <td>149.09</td>
    <td>149.13</td>
    <td>149.79</td>
    <td>148.96</td>
  </tr>
</table>


</body>

</html>