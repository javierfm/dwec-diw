<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Introducción a CSS</title>
    <link rel="stylesheet" type="text/css" href="../../css/styles.css">
</head>

<body>
<div id="cabecera">
    <a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
</div>
<div id="navegacion">
	<ul>
		<li><a href="../../index.html">Javier Fernández</a></li>
		<li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li class="activo">Introducción a CSS</li>
	</ul>
	<div style="clear: left"></div>
</div>
    <div id="contenedor">
        <h1>Introducción a CSS</h1>

        <div class="caja-ejercicio">
            <h2>Ejercicio 1</h2>
            <p>A partir del código HTML y CSS dado, añadir los selectores CSS que faltan para aplicar los estilos deseados. Cada regla CSS incluye un comentario en el que se explica los elementos a los que debe aplicarse:</p>
            <p><a href="ejercicio-01.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio de selectores</h2>
            <p>A partir del código HTML proporcionado, añadir las reglas CSS necesarias para que la página resultante tenga el mismo aspecto de una <a href="http://librosweb.es/img/css/e0201.png">imagen</a> que nos facilitan.</p>
			<p><a href="ejercicio-02.php">Ver ejercicio</a></p>
        </div>
		
        <div class="caja-ejercicio">
            <h2>Ejercicio 3</h2>
            <p>A partir del código HTML y CSS proporcionados, determinar las reglas CSS necesarias para añadir los siguientes <a href="http://librosweb.es/img/css/e0301.gif">márgenes y rellenos.</a></p>
       <p><a href="ejercicio-03.php">Ver ejercicio</a></p>
	    </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 4</h2>
            <p>A partir del código HTML y CSS proporcionados, determinar las reglas CSS necesarias para añadir los <a href="http://librosweb.es/img/css/e0302.gif"> siguientes bordes.</a></p>
       <p><a href="ejercicio-04.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 5</h2>
            <p>A partir del código HTML y CSS proporcionados, determinar las reglas CSS necesarias para añadir los <a href="http://librosweb.es/img/css/e0501.gif"> siguientes colores e imágenes de fondo.</a></p>
       <p><a href="ejercicio-05.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 6</h2>
            <p>A partir del código HTML determinar las reglas CSS necesarias para que el resultado sea similar al mostrado a la<a href="http://librosweb.es/img/css/f0512.gif"> imagen del enlace</a></p>
       <p><a href="ejercicio-06.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 7</h2>
            <p>A partir del código HTML y CSS proporcionados, determinar las reglas CSS necesarias para añadir las siguientes propiedades a la tipografía de la <a href="http://librosweb.es/img/css/e0701.gif">página</a></p>
       <p><a href="ejercicio-07.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 8</h2>
            <p>Definir las reglas CSS que permiten mostrar los enlaces con los siguientes estilos:</p>
            <ul>
                <li>En su estado normal, los enlaces se muestran de color rojo #CC0000.</li>
                <li>Cuando el usuario pasa su ratón sobre el enlace, se muestra con un color de fondo rojo #CC0000 y la letra de color blanco #FFF.</li>
                <li>Los enlaces visitados se muestran en color gris claro #CCC.</li>
            </ul>

       <p><a href="ejercicio-08.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 9</h2>
            <p>Definir las reglas CSS que permiten mostrar una galería de imágenes similar a la que se muestra en la <a href="http://librosweb.es/img/css/f0802.gif">imagen</a></p>
       <p><a href="ejercicio-09.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 10</h2>
            <p>Modificar el menú vertical sencillo para que muestre el comportamiento que nos indican.</p>
       <p><a href="ejercicio-10.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 11</h2>
            <p>Determinar las reglas CSS necesarias para mostrar la siguiente tabla con el <a href="http://librosweb.es/img/css/f1009.gif">aspecto final</a> (modificar el código HTML que se considere necesario añadiendo los atributos class oportunos).</p>
       <p><a href="ejercicio-11.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 12</h2>
            <p>Determinar las reglas CSS necesarias para mostrar la siguiente tabla con el <a href="http://librosweb.es/img/css/f1009.gif">aspecto final</a> (modificar el código HTML que se considere necesario añadiendo los atributos class oportunos).</p>
       <p><a href="ejercicio-12.php">Ver ejercicio</a></p>
        </div>

        <div class="caja-ejercicio">
            <h2>Ejercicio 13</h2>
            <p>Determinar las reglas CSS necesarias para mostrar la página HTML que se proporciona con el estilo que se muestra en la <a href="http://librosweb.es/img/css/e1301.gif"></a> imagen del enlace</p>
       <p><a href="ejercicio-13.php">Ver ejercicio</a></p>
        </div>

    </div>
</body>

</html>
