<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio 8</title>
    <!-- enlace a hoja de estilos generales Se desactivan para no interferir con los del ejercicio -->
    <!-- <link rel="stylesheet" type="text/css" href="../../css/styles.css"> -->

    <style type="text/css">

    #ejercicio{
      clear: left;
    }
    #ejercicio a {
      margin: 1em 0;
      float: left;
      clear: left;
      padding: 2px;
    }

    #ejercicio  a:hover {
      text-decoration: none;
      background-color: #CC0000;
      color: #FFF;
      }
    #ejercicio  a:visited {
      color: #CCC;
    }
</style>
 

<body>
<div id="navegacion">
  <ul>
    <li><a href="/miWebVirtual/">Javier Fernández</a></li>
    <li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li><a href="index.php">Introducción a CSS</a></li>
    <li class="activo">Ejercicio 8</li>
  </ul>
  <div style="clear: left"></div>
</div>

<div id="ejercicio">
  <a href="#">Enlace número 1</a>

<a href="#">Enlace número 2</a>

<a href="#">Enlace número 3</a>

<a href="#">Enlace número 4</a>

<a href="#">Enlace número 5</a>
</div>


</body>

</html>