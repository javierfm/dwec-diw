<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio posicionamiento float</title>
    <!-- enlace a hoja de estilos generales Se desactivan para no interferir con los del ejercicio -->
    <!-- <link rel="stylesheet" type="text/css" href="../../css/styles.css"> -->

    <link rel="stylesheet" type="text/css" media="screen" href="css/ejercicio06.css" />
 

<body><div id="navegacion">
  <ul>
    <li><a href="/miWebVirtual/">Javier Fernández</a></li>
    <li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
        <li><a href="index.php">Introducción a CSS</a></li>
    <li class="activo">Ejercicio 6</li>
  </ul>
  <div style="clear: left"></div>
</div>

<div id="navegacion2">
    <spam class="anterior">&laquo; Anterior</spam>  <spam class="siguiente">Siguiente &raquo;</spam>
    <div class="fix"></div>
  </div>

</body>

    <?php include("../../include/pie-pagina.php"); ?>
</div>

</body>

</html>