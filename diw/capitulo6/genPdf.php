<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../img/logo_fleming.jpg',10,8,33);
    // fuente
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
    $this->Cell(30,10,'Dr.Fleming IES',0,0,'C');
    // Salto de línea
    $this->Ln(20);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
}
}

// Creación del objeto de la clase heredada
//Vamos suponer uqe tenemos un Array de alumnos y que los queremos generar en PDF
$alumnos=array('Eloy Baizán','Javier Fernández','Diego Casaprima');
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
foreach($alumnos as $alumno){
    $pdf->Cell(0,10,utf8_decode($alumno),0,1);
}
$pdf->Output();
?>