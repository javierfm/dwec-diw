<?php include "includes/libchart/classes/libchart.php";?>
	<?php
    $chart = new PieChart();

	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point("Mozilla Firefox (80)", 80));
	$dataSet->addPoint(new Point("Konqueror (75)", 75));
	$dataSet->addPoint(new Point("Opera (50)", 50));
	$dataSet->addPoint(new Point("Safari (37)", 37));
	$dataSet->addPoint(new Point("Dillo (37)", 37));
	$dataSet->addPoint(new Point("Otros (72)", 70));
	$chart->setDataSet($dataSet);

	$chart->setTitle("Uso de navegadores");
	$chart->render("graficos/grafico1.png");
?>
		<!doctype html>
		<html lang="es">

		<head>
			<meta charset="UTF-8" />
			<title>enerar gráficos con las librerías para PHP LibChart</title>
			<!-- enlace a hoja de estilos generales -->
			<link rel="stylesheet" type="text/css" href="../../css/styles.css"> </head>

		<body>
			<div id="cabecera">
				<a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
			</div>
			<div id="navegacion">
				<ul>
					<li><a href="/miWebVirtual/">Javier Fernández</a></li>
					<li><a href="../index.html">Diseño de interfaces web (DIW)</a></li>
					<li><a href="index.html">Implementación de la usabilidad en la web. Diseño amigable.</a></li>
					<li class="activo">Generar gráficos con las librerías para PHP <strong>LibChart</strong></li>
				</ul>
				<div style="clear: left"></div>
			</div>
			<div id="contenedor">
				<h1>Generar gráficos con las librerías para PHP <strong>LibChart</strong></h1>
				<div> <img alt="Gráfico de sectores" src="graficos/grafico1.png" style="border: 1px solid gray;" /></div>
			</div>
		</body>

		</html>