<?php
require_once 'Conexion.php';
require_once 'Modulo.php';

class ModuloDAO{
    public function getModulos(){
        $listadoModulos=null;
        $conexion=Conexion::obtenerConexion();
        $statement=$conexion->prepare("SELECT * FROM AGA_MODULOS");
        while($rowObj=$statement->setFetchMode(PDO::FETCH_CLASS, 'Modulo')){
			$listadoModulos[]=$rowObj;
		}
        $conexion=null;//se cierra conexion
		return $listadoModulos;
    }
	
	public function getNombreModulo($codigoModulo){
		$nombreModulo=null;
		$conexion=Conexion::obtenerConexion();
		$query_params=["codigo_modulo"=>$codigoModulo];
        $statement=$conexion->prepare("SELECT NOMBRE FROM AGA_MODULOS WHERE CODIGO=:codigo_modulo");
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
        $nombreModulo=$resultado[0]['NOMBRE'];
		$conexion=null;
		return $nombreModulo;
	}
}