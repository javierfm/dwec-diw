<?php
require_once 'dbconfig.php';

class Conexion{
    public static function obtenerConexion(){
        $dsn="mysql:host=".SERVER_HOST.";dbname=".DATABASE_NAME;
        try{
            return new PDO($dsn,DATABASE_USER,DATABASE_PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8'));
        }
        catch (PDOException $ex){
            $ex->getMessage();
        }
    }
}