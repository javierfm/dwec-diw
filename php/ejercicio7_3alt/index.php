<?php
require 'modelo/ModuloDAO.php';
require 'modelo/NotasDAO.php';
require 'vista/Salida.php';
?>
	<?php

abstract class Index {
	//Diccionario que recibe valores del formulario. Por defecto, con la primera carga de la página están vacíos
    private static $valoresForm = array('error' => "", 'resultado' => "",'modulo'=>"");
	
	//Método que se lanza cuando se carga la página
    static function run() {
		$modulosDAO=new ModuloDAO();
		$notasDAO=new NotasDAO();
		
		var_dump($modulosDAO->getModulos());
		
		//Modulo seleccionado para que se muestre en plantilla
		self::$valoresForm['modulo']=$_POST['modulo'];
		
		//Se han recibido valores desde el formulario
        if (isset($_POST['nif'])) {
            $nif = $_POST['nif'];
            self::$valoresForm['nif'] = $nif;
			
			//NIF vacío
            if ($nif == NULL) {
				//Nif vacio, cambiamos parámetros
                self::$valoresForm['error'] = "Debe teclear un NIF";
            }
			else{
				//Nota del alumno
				$nota=$notasDAO->getNota($nif,$_POST['modulo']);
				//No hay nota
				if(count($nota)==0){
					self::$valoresForm['error'] = "El alumno no tiene nota";
				}
				//Hay nota
				else{
					self::$valoresForm['resultado'] = $nota;
				}
			}
        }
		//Llamamos a la vista para que saque por pantalla
        Salida::mostrarVista(self::$valoresForm);
    }
}
Index::run();
?>