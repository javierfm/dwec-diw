<?php

/* formulario.html */
class __TwigTemplate_e498636d53643a17683fc7fbd74e9795e5cd84971d3a8562449aa350f66f6159 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head lang=\"es\">
\t<meta charset=\"utf-8\">
\t<title>Consulta de notas</title>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"estilo.css\" /> </head>

<body>
\t<header> <img src=\"imagenes/logo_fleming.jpg\" id=\"logo\" />
\t\t<div id=\"daw\">Desenvolvimientu d'aplicaciones web</div>
\t</header>
\t<h1>Consulta de notes</h1>
\t<div id=\"central\">
\t\t<form action=\"\" method=\"post\">
\t\t\t<div class=\"linea\">
\t\t\t\t<label>NIF:</label>
\t\t\t\t<input type=\"text\" class=\"valor\" name=\"nif\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\" />
\t\t\t\t<label>Módulo:</label>
\t\t\t\t<select name=\"modulo\"> ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listaModulos"]) ? $context["listaModulos"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 21
            echo "\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" ";
            if (((isset($context["modulo"]) ? $context["modulo"] : null) == $context["key"])) {
                echo "selected ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</option> ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select>
\t\t\t</div>
\t\t\t<input class=\"boton\" type=\"submit\" value=\"Consultar\" /> </form>
\t\t<div class=\"linea\">
\t\t\t<div class=\"error\"> ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo " </div>
\t\t\t<div class=\"resultado\"> ";
        // line 26
        if (( !(null === (isset($context["resultado"]) ? $context["resultado"] : null)) &&  !twig_test_empty((isset($context["resultado"]) ? $context["resultado"] : null)))) {
            // line 27
            echo "\t\t\t\t<table>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Nota</th>
\t\t\t\t\t\t<th>Fecha nota</th>
\t\t\t\t\t</tr> ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["resultado"]) ? $context["resultado"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 32
                echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "NOTA", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "FECHA", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t</tr> ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo " </table>";
        }
        echo " </div>
\t\t</div>
\t</div>
</body>

</html>";
    }

    public function getTemplateName()
    {
        return "formulario.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 35,  89 => 34,  85 => 33,  82 => 32,  78 => 31,  72 => 27,  70 => 26,  66 => 25,  47 => 21,  43 => 20,  38 => 18,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "formulario.html", "D:\\Turno_manana\\XAMPP\\DAW\\php\\ejercicio7_3\\formulario.html");
    }
}
