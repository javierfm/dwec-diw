<?php

/* formulario.html */
class __TwigTemplate_1b2d104276547fe708e72fdb00b99b9aaced0352a6281ea70ac8b707333c165b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head>
\t<title>Consulta de notas</title>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../estilo.css\" /> </head>

<body>
\t<header> <img src=\"../logo_fleming.jpg\" id=\"logo\" />
\t\t<div id=\"daw\">Desenvolvimientu d'aplicaciones web</div>
\t</header>
\t<h1>Consulta de notes</h1>
\t<div id=\"central\">
\t\t<form action=\"\" method=\"post\">
\t\t\t<div class=\"linea\">
\t\t\t\t<label>NIF:</label>
\t\t\t\t<input type=\"text\" class=\"valor\" name=\"nif\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\" />
\t\t\t\t<label>Asignatura:</label>
\t\t\t\t<select name=\"asignatura\"> ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listaAsignaturas"]) ? $context["listaAsignaturas"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 21
            echo "\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</option> ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select>
\t\t\t</div>
\t\t\t<input class=\"boton\" type=\"submit\" value=\"Consultar\" /> </form>
\t\t<div class=\"linea\">
\t\t\t<div class=\"error\"> ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo " </div>
\t\t\t<div class=\"resultado\"> ";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["resultado"]) ? $context["resultado"] : null), "html", null, true);
        echo " </div>
\t\t</div>
\t</div>
</body>

</html>";
    }

    public function getTemplateName()
    {
        return "formulario.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 26,  62 => 25,  47 => 21,  43 => 20,  38 => 18,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "formulario.html", "E:\\Dropbox\\www\\DAW\\PHP\\ejercicio7_3\\formulario.html");
    }
}
