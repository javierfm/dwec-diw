<?php

/**
 * Clase modelo del ejercicio
 *
 * @author Ferja
 */

class Notas {
	
	//Asignaturas del ciclo
	public $asignaturas=array(
	'DAWS'=>"Desarrollo de aplicaciones web en servidor",
	'DAWC'=>"Desarrollo de aplicaciones web en cliente",
	'DIW'=>"Diseño de interfaces web",
	'DAW'=>"Despliegue de aplicaciones web"
	);
	
    //variable estatica, es compartida por todas las instancias de la clase
    private static $notasAlumnos = array(
    "9426830" => array(
        "DAWS" => 1,
        "DAWC" => 2,
        "DIW" => 3,
        "DAW" => 4
    ),
    "10421102" => array(
        "DAWS" => 4,
        "Desarrollo aplicaciones cliente" => 3,
        "DAWC" => 2,
        "DIW" => 1
    )
);

    //Devuelve la nota de un alumno ($nif) para una asignatura ($codigoAsignatura). Devuelve la nota o msg de error.
    public function leerNota($nif,$codigoAsignatura) {
        //al ser variable estática se hace referencia con self:: no con $this->
        if (isset(self::$notasAlumnos[$nif])) {
			$asignaturasAlumno=self::$notasAlumnos[$nif];
			if(isset($asignaturasAlumno[$codigoAsignatura])){
				return $asignaturasAlumno[$codigoAsignatura];
			}
			else{
				return "El alumno con el NIF ".$nif." no tiene ".$this->asignaturas[$codigoAsignatura];
			}
        }
        else{
            return "El alumno con el NIF ".$nif." no está matriculado";
        }
    }

}
?>