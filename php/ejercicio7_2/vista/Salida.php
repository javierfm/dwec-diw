<?php
/* La sentencia require_once es idéntica a require excepto que PHP verificará si el archivo ya ha sido incluido y si es así, no se incluye (require) de nuevo. */
require_once 'Twig/Autoloader.php';
?>
	<?php

/**
 * Vista del ejercicio.
 *
 * @author Ferja
 */
class Salida {

    //Genera HTML a partir de la plantilla y los valores recibidos en el form
    public static function mostrarVista($valores) {
        //El primer paso para utilizar Twig es registrar su cargador automático:
        Twig_Autoloader::register();
        /*
         * Twig uses a central object called the environment (of class Twig_Environment). 
         * Instances of this class are used to store the configuration and extensions, 
         * and are used to load templates from the file system or other locations.
         */
        $loader = new Twig_Loader_Filesystem('.');
        $enviroment = new Twig_Environment($loader, array('cache' => 'cache_compilacion', 'auto_reload' => true));
        //To load a template from a Twig environment, call the load() method which returns a Twig_TemplateWrapper instance:
        //$template=$enviroment->loadTemplate('formulario.html');
        //print $template->render($valores);
        //Se puede ahorrar un paso:
        print $enviroment->render('formulario.html', $valores);
    }

}