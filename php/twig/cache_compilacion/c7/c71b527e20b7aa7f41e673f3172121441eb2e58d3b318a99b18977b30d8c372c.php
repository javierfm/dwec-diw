<?php

/* formulario.html */
class __TwigTemplate_e498636d53643a17683fc7fbd74e9795e5cd84971d3a8562449aa350f66f6159 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
   <head>
      <title>Consulta de notas</title>
      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />
      <link rel=\"stylesheet\" type=\"text/css\" href=\"../estilo.css\" />
   </head>
   <body>
        <header>
           <img src=\"../logo_fleming.jpg\" id=\"logo\"/>    
           <div id=\"daw\">Desenvolvimientu d'aplicaciones web</div>
        </header>
        <h1>Consulta de notes</h1>
        <div id=\"central\">
          <form action=\"\" method=\"post\">
             <div class=\"linea\">
                 <label>NIF:</label>
                 <input type=\"text\" class=\"valor\" name=\"nif\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\" />
             </div>
             <input class=\"boton\" type=\"submit\" value=\"Consultar\" />
          </form>
          <div class=\"linea\">
             <div class=\"error\">
                ";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "
             </div>
             <div class=\"resultado\">
                ";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["resultado"]) ? $context["resultado"] : null), "html", null, true);
        echo "
             </div>
          </div>
        </div>
        <div id=\"pie\">
          Institutu d'educación secundaria \"Doctor Fleming\". Departamentu d'informática
        </div> 
   </body>
</html>";
    }

    public function getTemplateName()
    {
        return "formulario.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 27,  47 => 24,  38 => 18,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "formulario.html", "D:\\Turno_manana\\XAMPP\\DAW\\php\\twig\\formulario.html");
    }
}
