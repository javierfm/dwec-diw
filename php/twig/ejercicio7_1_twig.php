<?php
 /********************************************************************************************
  *  Ejercicio7_1, curso 2016/17.                                                            *
  * ******************************************************************************************/
   require_once '/Twig/Autoloader.php';

   class Formulario{     
      public static function mostrar($dic){
          Twig_Autoloader::register();
          $loader = new Twig_Loader_Filesystem('.');
          $twig = new Twig_Environment($loader, array(
                  'cache' => 'cache_compilacion','auto_reload' => true));
          print $twig->render('formulario.html', $dic);
      }
   }


   class Notas{
      private static $notas = array("11111111A" => 5,
                             "22222222B" => 7,
                             "33333333C" => 8,
                             "44444444D" => 10 );
      public function leerNota($alumno){
                if (isset(self::$notas[$alumno])){
                   return self::$notas[$alumno];
                } else {
                   return "L'alumnu con NIF: $alumno, nun tien nota";
                }
      }
   }
   
   abstract class Index{
        private static $diccionario = array('nif' => "", 'error' => "", 'resultado' => "");

        static function run(){
              $notas = new Notas();
              if (isset($_POST['nif'])){ 
                 $nif = $_POST['nif'];
                 self::$diccionario['nif'] = $nif;
                 if ($nif == null) {
                     self::$diccionario['error'] = 'Debe teclear un NIF';
                 } elseif (gettype($notas->leerNota($nif)) == 'string'){
                     self::$diccionario['error'] = $notas->leerNota($nif);
                 } else {
                     self::$diccionario['resultado'] =
                          "La nota del NIF $nif ye ".$notas->leerNota($nif);
                 }
              } 
              Formulario::mostrar(self::$diccionario);
        }
   }

   Index::run();
?>