<?php

/* login.html.twig */
class __TwigTemplate_1fa815303efc33d072af9569cfe4ba436c2942d555eb2a9c7bfcdd36e5f0f1ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("principal.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'tituloCab' => array($this, 'block_tituloCab'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Citas. Login";
    }

    // line 5
    public function block_tituloCab($context, array $blocks = array())
    {
        echo "Autenticación de usuario";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "    <form action=\"\" method=\"post\">
        <label for=\"usuario\" class=\"login\">Usuario:</label>
\t\t<input type=\"text\" class=\"login\" id=\"usuario\" name=\"usuario\" value = \"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["usuario"]) ? $context["usuario"] : null), "html", null, true);
        echo "\" required />
\t\t<label for=\"password\" class=\"login\">Contraseña:</label>
\t\t<input type=\"password\" class=\"login\" name=\"password\" id=\"password\" value=\"\" required />
        <input class=\"boton\" type=\"submit\" value=\"Enviar\" />
    </form>
    <div class=\"error\">";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 15,  49 => 10,  45 => 8,  42 => 7,  36 => 5,  30 => 3,);
    }
}
