<?php

/* citas.html.twig */
class __TwigTemplate_423072473c5721c2059a13123e6a82a1d13b979d7895ad5e2173e3b5059405c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("principal.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'tituloCab' => array($this, 'block_tituloCab'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Citas. Atencion";
    }

    // line 5
    public function block_tituloCab($context, array $blocks = array())
    {
        echo "Consultas de pacientes";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "    <form action=\"\" method=\"post\">
    \t<article class=\"selector\">  
            <div class=\"medico\">Doctor/a: 
                <span class=\"nombre\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "nombre", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape1", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape2", array()), "html", null, true);
        echo "</span>
            </div>
\t\t\t<label for=\"fecha\" class=\"citas\">Fecha: </label>
\t\t\t<select id=\"fecha\" name=\"fechaSel\" onchange=\"this.form.submit()\">
    \t\t\t";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "fechasCitas", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fecha"]) {
            echo " 
                \t<option value=\"";
            // line 16
            echo twig_escape_filter($this->env, $context["fecha"], "html", null, true);
            echo "\" ";
            echo ((($context["fecha"] == (isset($context["fechaSel"]) ? $context["fechaSel"] : null))) ? ("selected") : (" "));
            echo " />
                    \t  ";
            // line 17
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $context["fecha"], "d/m/Y"), "html", null, true);
            echo "
                 \t</option>
            \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fecha'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "            </select>
        </article>
        <table>
\t\t\t<tr><th>Paciente</th><th>Dia</th><th>Hora</th></tr>
\t\t\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["citas"]) ? $context["citas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["cita"]) {
            echo " 
                ";
            // line 25
            if (($this->getAttribute($context["cita"], "horaAtencion", array()) != null)) {
                // line 26
                echo "                    ";
                $context["atend"] = "atendida";
                // line 27
                echo "                    ";
                $context["title"] = ("Atendida a las " . twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "horaAtencion", array()), "H:i"));
                // line 28
                echo "                    ";
                $context["claseEnlace"] = "inactivo";
                // line 29
                echo "                ";
            } else {
                // line 30
                echo "                    ";
                $context["atend"] = " ";
                // line 31
                echo "                    ";
                $context["title"] = " ";
                // line 32
                echo "                    ";
                $context["claseEnlace"] = "activo";
                echo "    
                ";
            }
            // line 34
            echo "\t\t\t  \t<tr><td class=\"";
            echo twig_escape_filter($this->env, (isset($context["atend"]) ? $context["atend"] : null), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "\">
                        <a class=\"";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["claseEnlace"]) ? $context["claseEnlace"] : null), "html", null, true);
            echo "\"href=\"index.php?nif=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nifPaciente", array()), "html", null, true);
            echo "\">
                        ";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape1", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape2", array()), "html", null, true);
            echo "</a></td>
\t\t\t   \t    <td class=\"hora ";
            // line 37
            echo twig_escape_filter($this->env, (isset($context["atend"]) ? $context["atend"] : null), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                    <td class=\"hora ";
            // line 38
            echo twig_escape_filter($this->env, (isset($context["atend"]) ? $context["atend"] : null), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "H:i"), "html", null, true);
            echo "</td></tr>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cita'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "\t\t</table>
        <a class=\"boton\" href=\"cerrar.php\">Cerrar</a>
    </form>
";
    }

    public function getTemplateName()
    {
        return "citas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 40,  149 => 38,  141 => 37,  133 => 36,  127 => 35,  120 => 34,  114 => 32,  111 => 31,  108 => 30,  105 => 29,  102 => 28,  99 => 27,  96 => 26,  94 => 25,  88 => 24,  82 => 20,  73 => 17,  67 => 16,  61 => 15,  50 => 11,  45 => 8,  42 => 7,  36 => 5,  30 => 3,);
    }
}
