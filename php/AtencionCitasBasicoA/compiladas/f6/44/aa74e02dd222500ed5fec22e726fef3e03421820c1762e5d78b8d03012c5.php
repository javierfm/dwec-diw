<?php

/* citaAtendida.html.twig */
class __TwigTemplate_f644aa74e02dd222500ed5fec22e726fef3e03421820c1762e5d78b8d03012c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("principal.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'tituloCab' => array($this, 'block_tituloCab'),
            'head' => array($this, 'block_head'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Citas. Atención";
    }

    // line 5
    public function block_tituloCab($context, array $blocks = array())
    {
        echo "Atención al paciente";
    }

    // line 7
    public function block_head($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" type=\"text/css\" href=\"estilo/atencion.css\" />
";
    }

    // line 12
    public function block_contenido($context, array $blocks = array())
    {
        // line 13
        echo "    <form action=\"grabar.php\" method=\"post\">
    \t<div class=\"paciente\">Paciente: 
    \t\t<span class=\"nombre\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cita"]) ? $context["cita"] : null), "nombre", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cita"]) ? $context["cita"] : null), "ape1", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cita"]) ? $context["cita"] : null), "ape2", array()), "html", null, true);
        echo "</span>
        </div>
        <label for=\"hora\" class=\"atencion\">Hora atención:</label>
\t\t<input type=\"text\" class=\"atencion\" id=\"hora\" name=\"hora\" placeholder=\"hh:mm\" value = \"\" required />
\t\t<label for=\"diagnostico\" class=\"atencion\">Diagnóstico:</label>
\t\t<textarea class=\"atencion\" name=\"diagnostico\" id=\"diagnostico\" 
\t\t       rows=\"6\" cols=\"50\" required></textarea>
\t\t<label for=\"tratamiento\" class=\"atencion\">Tratamiento:</label>
\t\t<textarea class=\"atencion\" name=\"tratamiento\" id=\"tratamiento\" 
\t\t       rows=\"6\" cols=\"50\" required></textarea>
        <input class=\"boton\" type=\"submit\" value=\"Grabar\" />
    </form>
";
    }

    public function getTemplateName()
    {
        return "citaAtendida.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 15,  57 => 13,  54 => 12,  46 => 8,  43 => 7,  37 => 5,  31 => 3,);
    }
}
