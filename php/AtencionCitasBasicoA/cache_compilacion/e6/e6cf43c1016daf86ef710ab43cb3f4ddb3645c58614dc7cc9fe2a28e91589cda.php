<?php

/* principal.html.twig */
class __TwigTemplate_a6a51cc307288761c59b82141bf9cba218e3af77db57a738b7bb03e469df45ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'titulo' => array($this, 'block_titulo'),
            'tituloCab' => array($this, 'block_tituloCab'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
   <head>
      ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 9
        echo "   </head>
   <body>
        <header>
           <img src=\"imagenes/clinica_fleming.png\" id=\"logo\"/>    
           <div id=\"titulo\">";
        // line 13
        $this->displayBlock('tituloCab', $context, $blocks);
        echo "</div>
        </header>
        <article>";
        // line 15
        $this->displayBlock('contenido', $context, $blocks);
        echo "</article>
   </body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
          <link rel=\"stylesheet\" type=\"text/css\" href=\"estilo/estilo.css\" />
          <title>";
        // line 7
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>
      ";
    }

    public function block_titulo($context, array $blocks = array())
    {
    }

    // line 13
    public function block_tituloCab($context, array $blocks = array())
    {
    }

    // line 15
    public function block_contenido($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "principal.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  70 => 15,  65 => 13,  55 => 7,  51 => 5,  48 => 4,  41 => 15,  36 => 13,  30 => 9,  28 => 4,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "principal.html.twig", "D:\\Turno_manana\\XAMPP\\DAW\\php\\AtencionCitasBasicoA\\plantillas\\principal.html.twig");
    }
}
