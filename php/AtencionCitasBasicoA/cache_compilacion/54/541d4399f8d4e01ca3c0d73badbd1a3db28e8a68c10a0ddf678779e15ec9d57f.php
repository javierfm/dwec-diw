<?php

/* login.html */
class __TwigTemplate_403cea03de0a008ea10031c2068dccc9bd7a448e0dd15796a29bffb8ef95ef66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"estilo/estilo.css\" />
\t<title></title>
</head>

<body>
\t<header> <img src=\"imagenes/clinica_fleming.png\" id=\"logo\" />
\t\t<div id=\"titulo\"></div>
\t</header>
\t<form action=\"login.php\" method=\"post\">
\t\t<label for=\"usuario\" class=\"login\">Usuario:</label>
\t\t<input type=\"text\" class=\"login\" id=\"usuario\" name=\"usuario\" value=\"\" required />
\t\t<label for=\"password\" class=\"login\">Contraseña:</label>
\t\t<input type=\"password\" class=\"login\" name=\"password\" id=\"password\" value=\"\" required />
\t\t<input class=\"boton\" type=\"submit\" value=\"Enviar\" /> </form>
</body>

</html>";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.html", "D:\\Turno_manana\\XAMPP\\DAW\\php\\AtencionCitasBasicoA\\plantillas\\login.html");
    }
}
