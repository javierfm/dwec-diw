<?php

/* citas.html.twig */
class __TwigTemplate_8bed92c10240d1f9f9e653e00c1c1d7b2e69693421b735b89a8e3134de0faac5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", "citas.html.twig", 1);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'tituloCab' => array($this, 'block_tituloCab'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Citas. Atencion";
    }

    // line 5
    public function block_tituloCab($context, array $blocks = array())
    {
        echo "Consultas de pacientes";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "    <form action=\"\" method=\"post\">
    \t<article class=\"selector\">  
            <div class=\"medico\">Doctor/a: 
                <span class=\"nombre\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "nombre", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape1", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape2", array()), "html", null, true);
        echo "</span>
            </div>
\t\t\t<label for=\"fecha\" class=\"citas\">Fecha: </label>
\t\t\t<select id=\"fecha\" name=\"fechaSel\" onchange=\"this.form.submit()\">
    \t\t\t";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "fechasCitas", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fecha"]) {
            echo " 
                \t<option value=\"";
            // line 16
            echo twig_escape_filter($this->env, $context["fecha"], "html", null, true);
            echo "\" ";
            echo ((($context["fecha"] == (isset($context["fechaSel"]) ? $context["fechaSel"] : null))) ? ("selected") : (" "));
            echo " />
                    \t  ";
            // line 17
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $context["fecha"], "d/m/Y"), "html", null, true);
            echo "
                 \t</option>
            \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fecha'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "            </select>
        </article>
        <table>
\t\t\t<tr><th>Paciente</th><th>Dia</th><th>Hora</th></tr>
\t\t\t";
        // line 24
        $context["anotada"] = false;
        // line 25
        echo "\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["citas"]) ? $context["citas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["cita"]) {
            if ( !(isset($context["anotada"]) ? $context["anotada"] : null)) {
                // line 26
                echo "\t   \t   \t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["atendidas"]) ? $context["atendidas"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 27
                    echo "\t\t\t\t\t";
                    if ((($this->getAttribute($context["value"], "paciente", array()) == $this->getAttribute($context["cita"], "nifPaciente", array())) && ($this->getAttribute($context["value"], "fecha", array()) == $this->getAttribute($context["cita"], "fecha", array())))) {
                        // line 28
                        echo "               \t\t\t";
                        $context["anotada"] = true;
                        // line 29
                        echo "\t\t\t\t\t";
                    }
                    // line 30
                    echo "\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "\t\t   \t   ";
                if ((isset($context["anotada"]) ? $context["anotada"] : null)) {
                    // line 32
                    echo "\t   \t   \t\t\t\t<tr class=\"atendida\"><td><a class=\"inactivo\" href=\"anotar-cita.php?paciente=";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nifPaciente", array()), "html", null, true);
                    echo "&fecha=";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nombre", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape1", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape2", array()), "html", null, true);
                    echo "</a></td>
\t\t\t   \t   \t\t<td class=\"hora\">";
                    // line 33
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "d/m/Y"), "html", null, true);
                    echo "</td>
                 \t\t<td class=\"hora\">";
                    // line 34
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "H:i"), "html", null, true);
                    echo "</td></tr>
\t\t   \t   \t\t\t";
                } else {
                    // line 36
                    echo "\t\t\t\t\t\t<tr><td><a href=\"anotar-cita.php?paciente=";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nifPaciente", array()), "html", null, true);
                    echo "&fecha=";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nombre", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape1", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape2", array()), "html", null, true);
                    echo "</a></td>
\t\t\t   \t   \t\t<td class=\"hora\">";
                    // line 37
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "d/m/Y"), "html", null, true);
                    echo "</td>
                 \t\t<td class=\"hora\">";
                    // line 38
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["cita"], "fecha", array()), "H:i"), "html", null, true);
                    echo "</td></tr>
\t\t\t   ";
                }
                // line 40
                echo "\t\t\t\t   
\t\t\t";
                // line 41
                $context["anotada"] = false;
                // line 42
                echo "\t\t\t";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cita'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "\t\t</table>
        <a class=\"boton\" href=\"cerrar.php\">Cerrar</a>
    </form>
";
    }

    public function getTemplateName()
    {
        return "citas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 43,  168 => 42,  166 => 41,  163 => 40,  158 => 38,  154 => 37,  141 => 36,  136 => 34,  132 => 33,  119 => 32,  116 => 31,  110 => 30,  107 => 29,  104 => 28,  101 => 27,  96 => 26,  90 => 25,  88 => 24,  82 => 20,  73 => 17,  67 => 16,  61 => 15,  50 => 11,  45 => 8,  42 => 7,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "citas.html.twig", "D:\\Turno_manana\\XAMPP\\DAW\\php\\AtencionCitasBasicoA\\plantillas\\citas.html.twig");
    }
}
