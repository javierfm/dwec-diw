<?php
	session_start();
	require_once 'Twig/Autoloader.php';
	require_once 'modelo/citasDAO.php';
	
	$diccionario = array();

	Twig_Autoloader::register();
	$loader = new Twig_Loader_Filesystem('plantillas');
	$twig = new Twig_Environment($loader, array('cache' => 'cache_compilacion', 'auto_reload' => true));
	
	//EL usuario ya esta logeado
    if (isset($_SESSION['medico'])){
                header ("Location: login.php");
	}
	//Llegan valores de autentificacion desde login.html
	else if(isset($_POST['usuario']) && isset($_POST['password'])){
		$medico=citasDAO::leerMedico($_POST['usuario'],$_POST['password']);
		
		//No hay medico para esos datos
		if(!$medico){
			$diccionario['error']="No existe médico con esos credenciales";
			$plantilla = $twig->loadTemplate('login.html');
            print $plantilla->render($diccionario);
		}
		//Hay médico, lo metemos en sesion
		else{
			$_SESSION['medico']=$medico;
			//Redireccionamos a index.php para gestione
			header ("Location: index.php");
		}
	}
	//Acceso limpio
	else{

            $plantilla = $twig->loadTemplate('login.html');
            print $plantilla->render($diccionario);
	}
?>