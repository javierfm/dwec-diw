<?php
include_once'Medico.php';

class CitasDAO{
	private static $conexion = null; 

    public static function conexion(){
		if (self::$conexion == null){
          try {
            /*
            self::$conexion = new PDO('oci:dbname=//hpproliant.fleming.as:1521/orclhp.fleming.as;charset=AL32UTF8', 
                                                       ' ', ' ', array(PDO::ATTR_PERSISTENT => true)); 
            self::$conexion->exec("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH:MI:SS'");*/
            
            self::$conexion = new PDO('mysql:host=192.168.8.70;dbname=examen', 'examen', "examen",
                                                     array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
          } catch(PDOException $ex){
            echo 'No se pudo conectar con la base de datos<br>';
            die();
          }
			    
		}
			return self::$conexion;	   
    }

	
     public static function leerMedico($login,$password){
        $gbd = self::conexion(); 
        $consulta = "SELECT NIF as \"nif\", APE1 as \"ape1\", APE2 as \"ape2\", 
                            NOMBRE as \"nombre\", LOGIN as \"login\", PASSWORD as \"password\" 
                     FROM MEDICOS 
                     WHERE LOGIN = :login AND PASSWORD=:password";
        $resultado = $gbd->prepare($consulta);
        $resultado->bindParam(':login', $login, PDO::PARAM_STR, 10);
		$resultado->bindParam(':password', $password);
        $resultado->execute();
        $medico = $resultado->fetchObject("Medico"); 
        $resultado->closeCursor();
		 //si existe ese usuario ya obtiene las citas
		 var_dump($medico);
        if ($medico != null){
        	$fechas = self::leerFechas($medico->getNif());
        	$medico->setFechasCitas($fechas);
        }
        $gbd = null;
        return $medico;
    }

    private static function leerFechas($medico){ 
        $gbd = self::conexion(); 
        $consulta = "SELECT FECHA FROM CITAS 
                     WHERE NIF_MEDICO = :medico
                     ORDER BY FECHA";
        $resultado = $gbd->prepare($consulta);
        $resultado->bindParam(':medico', $medico, PDO::PARAM_STR, 10);
        $resultado->execute();
        $fechas = $resultado->fetchAll(PDO::FETCH_ASSOC); 
        $resultado->closeCursor();
        $gbd = null;
        $salida = array();
        foreach ($fechas as $f) {
        	$fecha = (new DateTime($f['FECHA']))->format('Y-m-d'); 
        	if (array_search($fecha, $salida) === FALSE){
        		$salida[] = $fecha;
        	}
        }  
        return $salida;
    }

    public static function leerCitas($medico, $fecha){ 
        $gbd = self::conexion(); 
        $consulta = "SELECT NIF_PACIENTE as \"nifPaciente\", NIF_MEDICO as \"nifMedico\",
                            FECHA as \"fecha\", APE1 as \"ape1\", APE2 as \"ape2\", 
                            NOMBRE as \"nombre\"
                     FROM CITAS
                     WHERE NIF_MEDICO = :medico
                     ORDER BY FECHA";
        $resultado = $gbd->prepare($consulta);
        $resultado->bindParam(':medico', $medico, PDO::PARAM_STR, 10);
        $resultado->execute();
        $citas = $resultado->fetchAll(PDO::FETCH_CLASS, 'Cita'); 
        $resultado->closeCursor();
        $gbd = null; 
        $salida = array();
        foreach ($citas as $c) {
        	if ((new DateTime($c->getFecha()))->format('Y-m-d') == $fecha){
        		$salida[] = $c;
        	}
        } 
        return $salida;
    }

}
?>