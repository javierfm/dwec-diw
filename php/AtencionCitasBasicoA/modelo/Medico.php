<?php

class Medico {
	private $nif;
	private $nombre;
	private $ape1;
	private $ape2;
	private $login;
	private $password;
	private $fechasCitas;

	public function getNif() {
		return $this->nif;
	}

	public function getNombre() {
		return $this->nombre;
	}

	public function getApe1() {
		return $this->ape1;
	}

	public function getApe2() {
		return $this->ape2;
	}

	public function getLogin() {
		return $this->login;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getFechasCitas(){
		return $this->fechasCitas;
	}

	public function setFechasCitas($fechas){
		$this->fechasCitas = $fechas;
	}
}
?>