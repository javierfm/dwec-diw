<?php

class Cita {
	private $nifPaciente;
	private $nifMedico;
	private $fecha;
	private $nombre;
	private $ape1;
	private $ape2;
	private $atendida;

	public function getNifPaciente() {
		return $this->nifPaciente;
	}

	public function getNifMedico() {
		return $this->nifMedico;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function getNombre() {
		return $this->nombre;
	}

	public function getApe1() {
		return $this->ape1;
	}

	public function getApe2() {
		return $this->ape2;
	}
	
	public function setAtendida($atendida){
		$this->atendida=$atendida;
	}
	
	public function getAtendida(){
		return $this->atendida;
	}
}
?>