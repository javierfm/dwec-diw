<?php
	require_once 'Twig/Autoloader.php';
	require_once 'modelo/Cita.php';
	require_once 'modelo/citasDAO.php';
	require_once 'modelo/Medico.php';
   
abstract class Index{
        private static $diccionario = array();

        static function run(){
            session_start();
			
			//Usuario no se ha logeado
            if (!isset($_SESSION['medico'])){ 
                header ("Location: login.php");
            }
			
            
			//Este médico se mete en sesion desde la pantalla de login.php
            self::$diccionario['medico'] = $_SESSION['medico'];
			
			//La fecha llega desde POST
			//Llega desde citas
            if (isset($_POST['fechaSel'])){
                self::$diccionario['fechaSel'] = $_POST['fechaSel'];
                $_SESSION['fechaSel'] = $_POST['fechaSel'];
            } 
			//La fecha llega desde session
			elseif (isset($_SESSION['fechaSel'])) {
                self::$diccionario['fechaSel'] = $_SESSION['fechaSel'];
            } 
			//Se toma la primera fecha por defecto
			//Primera vez que se  carga la pagina sin ninguna fecha en sesion
			else {
                self::$diccionario['fechaSel'] = $_SESSION['medico']->getFechasCitas()[0];
            }
			
            $citas = CitasDAO::leerCitas($_SESSION['medico']->getNif(), self::$diccionario['fechaSel']); 
			
			
            self::$diccionario['citas'] = $citas;
			if(isset($_SESSION['atendidas'])){
				self::$diccionario['atendidas']=$_SESSION['atendidas'];
			}
			
            $_SESSION['citas'] = $citas;
			
        	Twig_Autoloader::register();
			$loader = new Twig_Loader_Filesystem('plantillas');
			$twig = new Twig_Environment($loader, array('cache' => 'cache_compilacion', 'auto_reload' => true));
            $plantilla = $twig->loadTemplate('citas.html.twig');
            print $plantilla->render(self::$diccionario);
        }
   }

   Index::run();
?>