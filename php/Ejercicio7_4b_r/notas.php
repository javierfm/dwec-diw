<?php
 /********************************************************************************************
  *  Ejercicio7_4 (solucion b), curso 2016/17.                                               *
  * ******************************************************************************************/
   include 'cargadores.php';  

   abstract class Notas{
        private static $diccionario = array();
        private static $codModulo = "";

        static function mostrarNotas(){
              $cgd = new Cargadores();

              // Si no hay cookie enviarlo a index.php
              if (($nif = $_COOKIE['nif']) == null){//En el array de cookies busca en elemento de clave nif. si es nulo es que no existe
                  header ("Location: index.php");
              }
              self::$diccionario['nif'] = $nif;
              self::$codModulo = isset($_POST['modulo']) ? $_POST['modulo'] : "";
              $alumno = new Alumno($nif); 
              self::$diccionario['alumno'] = $alumno->getNombre().' '.
                                             $alumno->getApellido1().' '.
                                             $alumno->getApellido2();  
              $notas = $alumno->getNotas();
              self::cargarModulos($notas);  
              self::$diccionario['notas'] = $alumno->getNotasModulo(self::$codModulo);    
              
              $cgd->respuesta("notas.html.twig", self::$diccionario);
        }
   
        private static function cargarModulos($notas){
              $modulos = array();
              foreach ($notas as $nota) { 
                if (!array_key_exists ($nota->getCodModulo(), $modulos)){
                    $modulos[$nota->getCodModulo()] = $nota->getNomModulo(); 
                }
              } 
              if (self::$codModulo == ""){
                  self::$codModulo = current(array_keys($modulos));
              }
              self::$diccionario['modulos'] = $modulos;
              self::$diccionario['moduloSel'] = self::$codModulo;
        }

   }
   Notas::mostrarNotas();
?>