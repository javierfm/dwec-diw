<?php
 /********************************************************************************************
  *  Ejercicio7_4 (solucion b), curso 2016/17.                                                            *
  * ******************************************************************************************/
/*
Sirve le formulario primero y controla qu todo esta bien. luego redirecciona a notas
No hay redirecciones explicitas como Java
*/
   include 'cargadores.php';

   abstract class Index{
        private static $diccionario = array('nif' => "", 'error' => "");

        static function run(){
            $cgd = new Cargadores();
            
            $forma = 'formulario';
            if (isset($_POST['nif'])){ 
                $nif = strtoupper(trim($_POST['nif'])); 
                self::$diccionario['nif'] = $nif;
                if ($nif == null) {
                    self::$diccionario['error'] = 'Tien que teclear un NIF';
                } else {
                    $alumno = new Alumno($nif); //Va a la BBDD lee el alumno y carga sus propiedades
                    if ($alumno->getNombre() == null){
                        self::$diccionario['error'] = "El alumnu nun ta rexistrau";
                    } elseif ($alumno->tieneNotas()) {//Se llama a metodo que devuelve TRUE si tiene notas y FALSE si no tiene.
						//como va en cabecera tiene que ir antes que la respuesta (linea 37)
                        setcookie("nif", $nif);//MIRAR LA DOCUMENTACION QUE HACE ESTO. Sino pongo parametro es una cookie de sesion. Tiempo negativo la anulas
						//Siempre que se escriba en la cabecera hay que escribirlo antes del cuerpo porque sino da error.
                        header ("Location: notas.php");//Hace redirección. Escribe en la cabecera de la respuesta, decirle al navegador donde tiene que ir
                    } else {
                        self::$diccionario['error'] = 'El alumnu nun tien notes en ningún módulu.';
                    }
                }
            } 
            $cgd->respuesta("formulario.html.twig", self::$diccionario);//respuesta()esta en cargadores, que fue donde se instanció todo
        }
   }
   Index::run();
?>