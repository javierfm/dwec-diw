<?php
 /**************************************************************************
  *  Esta clase pertenece al MODELO del ejercicio7_4. Curso 2016/17.       *
  * ************************************************************************/
   class Conexion{
      private static $conexion = null; 
      public static function obtenerConexion(){
			if (self::$conexion == null){
          libxml_use_internal_errors(true);
          if(!$xml = simplexml_load_file('conexion.xml')){
            print "Error abriendo el fichero con los datos de conexión.<br/>";
            foreach(libxml_get_errors() as $error) {
                  echo $error->message.'<br/>';
            }
            die();
          } 
          $opciones = array(PDO::ATTR_PERSISTENT => true);
          if (substr_count($xml->dsn, 'mysql') > 0){ 
              $opciones[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';
          } 
          try {
                self::$conexion = new PDO($xml->dsn, $xml->usuario, $xml->password, $opciones); 
                if (substr($xml->dsn, 0, 3) == "oci"){
                    self::$conexion->exec("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD'");
                }
          } catch (PDOException $e) {
                print "<p>Error: No puede conectarse con la base de datos.</p>\n";
                print "<p>Error: ".$e->getMessage()."</p>\n";
                die();
          }
			}
			return self::$conexion;	   
      }
   }