<?php
 /**************************************************************************
  *  Esta clase pertenece al MODELO del ejercicio7_4. Curso 2016/17.      *
  * ************************************************************************/
   class Nota{
      private $codModulo = "";
      private $nomModulo = "";
      private $fecha = "";
      private $calificacion = "";

      public function getCodModulo(){
            return $this->codModulo;
      }

      public function getNomModulo(){
            return $this->nomModulo;
      }

      public function getFecha(){
         $fecha = new DateTime($this->fecha); 
            return $fecha->format('d/m/Y');
      }

      public function getCalificacion(){
            return $this->calificacion;
      }
   }