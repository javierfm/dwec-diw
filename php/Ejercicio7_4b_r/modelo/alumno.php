<?php
 /*********************************************************************
  *  Esta clase pertenece al MODELO del ejercicio7_4. Curso 2016/17.  *
  * *******************************************************************/
  
   class Alumno{
      private $nif = "";
      private $nombre = "";
      private $apellido1 = "";
      private $apellido2 = "";
      private $notas = array();

      public function __construct($nif){
            $this->nif = $nif;
            $conexion = Conexion::obtenerConexion(); 
            $consulta = "SELECT NOMBRE, APE1, APE2
                         FROM AGA_ALUMNOS 
                         WHERE NIF = :nifAlumno";
            $sentencia = $conexion->prepare($consulta);
            $sentencia->bindParam(':nifAlumno', $nif, PDO::PARAM_STR, 9);
            $sentencia->execute();
            $fila = $sentencia->fetch(); 
            if ($fila){ 
              $this->nombre = $fila[0]; 
              $this->apellido1 = $fila[1];
              $this->apellido2 = $fila[2];
              $this->leerNotas(); 
            }
            $sentencia->closeCursor();
            $conexion = null;
      }

      public function getNif(){
            return $this->nif;
      }

      public function getNombre(){
            return $this->nombre;
      }

      public function getApellido1(){
            return $this->apellido1;
      }

      public function getApellido2(){
            return $this->apellido2;
      }
      
      public function getNotas(){
            return $this->notas;
      }

      public function getNotasModulo($modulo){
            $respuesta = array();
            foreach ($this->notas as $nota) { 
               if ($nota->getCodModulo() == $modulo){
                   $respuesta[] = $nota;
               }
            }
            return $respuesta;
      }
            
      private function leerNotas(){
            $gbd = Conexion::obtenerConexion(); 
            $consulta = 'SELECT CODIGO as "codModulo",
                                NOMBRE as "nomModulo", 
                                FECHA as "fecha",
                                NOTA as "calificacion"
                         FROM AGA_NOTAS, AGA_MODULOS 
                         WHERE CODIGO = COD_MODULO AND
                               NIF_ALU = :nifAlumno
                         ORDER BY FECHA DESC';
            $sentencia = $gbd->prepare($consulta);
            $sentencia->setFetchMode( PDO::FETCH_CLASS, 'Nota');
            $sentencia->execute(array(':nifAlumno'=>$this->nif)); 
            while($n = $sentencia->fetch(PDO::FETCH_CLASS)) {
               $this->notas[] = $n;
            }
            $sentencia->closeCursor();
            $gbd = null; 
      }

      public function tieneNotas(){
            return count($this->notas);
      }
   }