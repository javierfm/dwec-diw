<?php
require  'vista/Salida.php';
require 'modelo/Notas.php';
?>
	<?php

abstract class Index {

//Diccionario que recibe valores del formulario. Por defecto, con la primera carga de la página están vacíos
    private static $valoresForm = array('nif' => "", 'error' => "", 'resultado' => "");

//Método que se lanza cuando se carga la página
    static function run() {
//instanciamos modelo
        $notas = new Notas();
//Se han recibido valores desde el formulario
        if (isset($_POST['nif'])) {
            $nif = $_POST['nif'];
            self::$valoresForm['nif'] = $nif;
//NIF vacío
            if ($nif == NULL) {
//Nif vacio, cambiamos parámetros
                self::$valoresForm['error'] = "Debe teclear un NIF";
            }
//No hay nota (el método devuelve un string correspondiente al error)
            elseif (gettype($notas->leerNota($nif)) == 'string') {

                self::$valoresForm['error'] = $notas->leerNota($nif);
            }
//todo ok, hay nota
            else {
                self::$valoresForm['resultado'] = "La nota para el NIF " . $nif . " es " . $notas->leerNota($nif);
            }
        }
		//Llamamos a la vista para que saque por pantalla
            Salida::mostrarVista(self::$valoresForm);
    }
}
Index::run();
?>