<?php
 /********************************************************************************************
  *  Ejercicio7_4 (solucion b), curso 2016/17.                                                            *
  * ******************************************************************************************/
/*
Sirve le formulario primero y controla qu todo esta bien. luego redirecciona a notas
No hay redirecciones explicitas como Java
*/

session_start();//Iniciar una nueva sesión o reanudar la existente
   include 'cargadores.php';

   abstract class Index{
        private static $diccionario = array('nif' => "", 'error' => "");

        static function run(){
            $cgd = new Cargadores();
			
			/* La mejor forma es hacer un nuevaConsulta.php, matar la sesion y redireccionar*/
			//Compruebo que no vengo desde notas->nueva consulta
			/*if(isset($_GET['reset'])){
				//borro la variable de session nif
				session_unset();
			}*/
            
            
			//Compruebo si ya hay un nif en la sesion para redireccionar
			if(isset($_SESSION['nif'])){
				header("Location:notas.php");//Va a conulta de notas directamente
			}
			
			$forma = 'formulario';
            if (isset($_POST['nif'])){ 
                $nif = strtoupper(trim($_POST['nif'])); 
                self::$diccionario['nif'] = $nif;
                if ($nif == null) {
                    self::$diccionario['error'] = 'Tien que teclear un NIF';
                } else {
                    $alumno = new Alumno($nif); //Va a la BBDD lee el alumno y carga sus propiedades
                    if ($alumno->getNombre() == null){
                        self::$diccionario['error'] = "El alumnu nun ta rexistrau";
                    } elseif ($alumno->tieneNotas()) {//Se llama a metodo que devuelve TRUE si tiene notas y FALSE si no tiene.
						//Meto en la sesion el nif
						$_SESSION['nif']=$nif;
						//como va en cabecera tiene que ir antes que la respuesta (linea 37)
                        //setcookie("nif", $nif,time()+60*2);//Dos minutos de cookie
						//Siempre que se escriba en la cabecera hay que escribirlo antes del cuerpo porque sino da error.
                        header ("Location: notas.php");//Hace redirección. Escribe en la cabecera de la respuesta, decirle al navegador donde tiene que ir
                    } else {
                        self::$diccionario['error'] = 'El alumnu nun tien notes en ningún módulu.';
                    }
                }
            } 
            $cgd->respuesta("formulario.html.twig", self::$diccionario);//respuesta()esta en cargadores, que fue donde se instanció todo
        }
   }
   Index::run();
?>