<?php
 /********************************************************************************************
  *  Cargadores Twig y propio.                                                        *
  * ******************************************************************************************/
/*
Como los utilizan dos cargadores los monta todos en este
*/
   include 'Autoloader.php';//Cargador de Twig
   include 'autocargador.php';//Home made

   Class Cargadores{
      private $twig;
	   /* inicializo los 2 autoloaders*/
   		public function __construct(){
   			    Twig_Autoloader::register();//inicializo el de twig
            $loader = new Twig_Loader_Filesystem('plantillas');
            $this->twig = new Twig_Environment($loader, array(
                          'cache' => 'compiladas','auto_reload' => true));

            Autocargador::registrar();//actualizo el cargador casero
            Autocargador::setDirectorios("modelo");//directorios donde buscar
   		}

      public function respuesta($plantilla, $datos){
          $plantilla = $this->twig->loadTemplate($plantilla);
          print $plantilla->render($datos);
      }
   }