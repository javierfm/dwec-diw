<?php

/* formulario.html.twig */
class __TwigTemplate_cf9a25a531c137d55317c10219fe3cbb0f0ff127ec1f14368999738878b128b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("principal.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Selección alumno.";
    }

    // line 5
    public function block_contenido($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Consulta de notes</h1>
    <form action=\"\" method=\"post\">
        <div class=\"linea\">
            <label>NIF:</label>
            <input type=\"text\" name=\"nif\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\" />
        </div>
        <input class=\"boton\" type=\"submit\" value=\"Consultar\" />
    </form>
    <div class=\"error\">";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "formulario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 14,  44 => 10,  38 => 6,  35 => 5,  29 => 3,);
    }
}
