<?php
 /********************************************************************************************
  *  Ejercicio7_4c, curso 2016/17.                                                            *
  * ******************************************************************************************/
   include 'Autoloader.php';
   include 'funcionesDAO.php';

   /*
El procesador php recupera los datos de session, mira si en la peticion hay una cookie de sesion, si no hay cookie session_start() 
crea un id de sesion y se envia en la respuesta. Al cerrar el navegador la cookie se deshecha.
Si hay cookie recupera los objetos en php.ini hay un parámetro que indica donde guarda las sesiones session.save_path
Serializa-Deserializa
   */
   session_start();//Si no se pone este método no se puede hacer uso de sesiones
   /*
   $_SESSION -> Array con variables, accedemos igual que un array normal
   */
   if (!isset($_SESSION['nif'])){//No se paso por la parte de validar NIF
   /* coloca info en la cabecera de la respuesta.
   Se tiene que poner antes de construir antes que el cuerpo de la respuesta sino da error
   */
      header ("Location: capturaNif.php");//redirecciono a capturar el nif
   }  

   $diccionario = array();
   $codModulo = "";

   $nif = $_SESSION['nif'];
   $notas = $_SESSION['notas'];
   $alumno = $_SESSION['alumno'];
   $diccionario['nif'] = $nif;
   $codModulo = isset($_POST['modulo']) ? $_POST['modulo'] : "";
   $diccionario['alumno'] = $alumno;   
   cargarModulos($notas);  
   $diccionario['notas'] = cargarNotas($notas); 

   Twig_Autoloader::register();
   $loader = new Twig_Loader_Filesystem('plantillas');
   $twig = new Twig_Environment($loader, array(
                          'cache' => 'compiladas','auto_reload' => true));   
   print $twig->render("notas.html.twig", $diccionario);  

   function cargarModulos($notas){
        global $codModulo, $diccionario;
        $modulos = array();
        foreach ($notas as $nota) { 
          if (!array_key_exists ($nota['codModulo'], $modulos)){
              $modulos[$nota['codModulo']] = $nota['nomModulo']; 
          }
        } 
        if ($codModulo == ""){
            $codModulo = current(array_keys($modulos));
        }
        $diccionario['modulos'] = $modulos;
        $diccionario['moduloSel'] = $codModulo;
   }

   function cargarNotas($notas){
        global $codModulo;
        $notasModulo = array();
        foreach ($notas as $nota) { 
          if ($nota['codModulo'] == $codModulo){
              $notasModulo[] = array( "fecha"=>$nota['fecha'], "calificacion"=>$nota['calificacion']);
          }
        }
        return $notasModulo;
   }

?>
