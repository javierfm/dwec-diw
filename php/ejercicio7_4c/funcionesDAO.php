<?php
 /*********************************************************************
  *  Funciones que permiten el aceso a los objetos dela base de datos.*
  * *******************************************************************/
      static $bd = null;

      function leerAlumno($nif){
            global $bd;
            $alu = "";
		  //Si existe un PDO lo obtiene
            if ($bd == null) {
                $bd = obtenerConexion();
            }
            $consulta = "SELECT NOMBRE, APE1, APE2
                         FROM AGA_ALUMNOS 
                         WHERE NIF = :nifAlumno";
		  /*
		  Prepara una sentencia SQL para ser ejecutada por el método PDOStatement::execute(). La sentencia SQL puede contener cero o más marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada. No se pueden usar marcadores de parámetros con nombre y signos de interrogación en la misma sentencia SQL; se debe elegir uno de los dos estilos de parámetros. Se deben usar estos parámetros para sustituir cualquier dato de usuario, y no usarlos directamente en la consulta.*/
           
		  //PDOStatement
		  $sentencia = $bd->prepare($consulta);
		  
		  /*
		  Llamar a PDO::prepare() y a PDOStatement::execute() para sentencias que serán ejecutadas en múltiples ocasiones con diferentes parámetros optimiza el rendimiento de la aplicación permitiendo al driver negociar en lado del cliente y/o servidor el almacenamiento en caché del plan de consulta y meta información, y ayuda a prevenir inyecciones SQL eliminando la necesidad de entrecomillar manualmente los parámetros.*/
		  
            $sentencia->bindParam(':nifAlumno', $nif, PDO::PARAM_STR, 9);//Vincula un parámetro al nombre de variable especificado
            $sentencia->execute();//Ejecuta una sentencia preparada
            $fila = $sentencia->fetch(PDO::FETCH_ASSOC); //Obtiene la siguiente fila de un conjunto de resultados
            if ($fila){ 
              $alu = $fila['NOMBRE']." ".$fila['APE1']." ".$fila['APE2'];
            }
            $sentencia->closeCursor();//Cierra un cursor, habilitando a la sentencia para que sea ejecutada otra vez
            $bd = null;
            return $alu;
      }

      function leerNotas($nif){
            $notas = array();
            global $bd;
            if ($bd == null) {
                $bd = obtenerConexion();
            }
            $consulta = 'SELECT CODIGO as "codModulo",
                                NOMBRE as "nomModulo", 
                                FECHA as "fecha",
                                NOTA as "calificacion"
                         FROM AGA_NOTAS, AGA_MODULOS 
                         WHERE CODIGO = COD_MODULO AND
                               NIF_ALU = :nifAlumno
                         ORDER BY FECHA DESC';
            $sentencia = $bd->prepare($consulta);
            $sentencia->execute(array(':nifAlumno'=>$nif)); 
            $notas = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $gbd = null; 
            return $notas;
      }

      function tieneNotas($nif){
            global $bd;
            
            if ($bd == null) {
                $bd = obtenerConexion();
            }
            $consulta = 'SELECT count(*) FROM AGA_NOTAS
                         WHERE  NIF_ALU = :nifAlumno';
            $sentencia = $bd->prepare($consulta);
            $sentencia->execute(array(':nifAlumno'=> $nif)); 
            $cuenta = $sentencia->fetchColumn();
            $sentencia->closeCursor();
            $bd = null; 
            return $cuenta;
      }
//Recupera los datos para la conexion a la BBDD
      function leerDatosConexion(){
          libxml_use_internal_errors(true);
          if(!$xml = simplexml_load_file('conexion.xml')){
            print "Error abriendo el fichero con los datos de conexión.<br/>";
            foreach(libxml_get_errors() as $error) {
                  echo $error->message.'<br/>';
            }
            die();
          }
          return array("dsn" => $xml->dsn,
                       "usuario" => $xml->usuario,
                       "password" => $xml->password);
      }

// Devuelve un objeto PDO
      function obtenerConexion(){
          $datosConexion = leerDatosConexion();
          $opciones = array(PDO::ATTR_PERSISTENT => true);//Solicita una conexión persistente, en vez de crear una nueva conexión
          if (substr_count($datosConexion['dsn'], 'mysql') > 0){ 
              $opciones[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';//Valores que se reciben desde BBDD en UTF-8
          } 
          try {
			  //Instancia un objeto PDO y lo devuelve
                $gbd = new PDO($datosConexion['dsn'],
                               $datosConexion['usuario'],
                               $datosConexion['password'],
                               $opciones); 
                return $gbd;
          } catch (PDOException $e) {
                print "<p>Error: No puede conectarse con la base de datos.</p>\n";
                print "<p>Error: ".$e->getMessage()."</p>\n";
                die();
          }
      }