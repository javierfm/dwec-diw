<?php

/* notas.html.twig */
class __TwigTemplate_86a4f38109a666e5f304dfd903cb4efaf173f5a19e835b1d276b567ba2fca4dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", "notas.html.twig", 1);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Consulta de notes";
    }

    // line 5
    public function block_contenido($context, array $blocks = array())
    {
        // line 6
        echo "        <h1>Consulta de notes</h1>
           <div class=\"linea2\">
             Alumno: ";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["alumno"]) ? $context["alumno"] : null), "html", null, true);
        echo "
           </div>
           <form action=\"\" method=\"post\">
              <!--<input type=\"hidden\" name=\"nif\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\"/>-->
              <ul>
              ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modulos"]) ? $context["modulos"] : null));
        foreach ($context['_seq'] as $context["codigo"] => $context["nombre"]) {
            echo " 
                 <li><input type=\"radio\" name=\"modulo\" onchange=\"this.form.submit()\" 
                            value=\"";
            // line 15
            echo twig_escape_filter($this->env, $context["codigo"], "html", null, true);
            echo "\" ";
            echo ((($context["codigo"] == (isset($context["moduloSel"]) ? $context["moduloSel"] : null))) ? ("checked") : (" "));
            echo " />
                      ";
            // line 16
            echo twig_escape_filter($this->env, $context["nombre"], "html", null, true);
            echo "
                 </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['codigo'], $context['nombre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "              </ul>
           </form>
            <table><tr><th>Fecha</th><th>Nota</th></tr>
            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["notas"]) ? $context["notas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["nota"]) {
            // line 23
            echo "                   <tr><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["nota"], "fecha", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["nota"], "calificacion", array()), "html", null, true);
            echo "</td></tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nota'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "            </table>
           <a class=\"boton\" href=\"cerrarSesion.php\">Nueva Consulta</a>
";
    }

    public function getTemplateName()
    {
        return "notas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 25,  86 => 23,  82 => 22,  77 => 19,  68 => 16,  62 => 15,  55 => 13,  50 => 11,  42 => 8,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "notas.html.twig", "D:\\Turno_manana\\XAMPP\\DAW\\php\\ejercicio7_4c\\plantillas\\notas.html.twig");
    }
}
