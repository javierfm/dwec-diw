<?php

/* formulario.html.twig */
class __TwigTemplate_922a0398600463cb3494106d66b4db215b86aeba0e3149c34a66d1959c61f875 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", "formulario.html.twig", 1);
        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        echo "Selección alumno.";
    }

    // line 5
    public function block_contenido($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Consulta de notes</h1>
    <form action=\"\" method=\"post\">
        <div class=\"linea\">
            <label>NIF:</label>
            <input type=\"text\" name=\"nif\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
        echo "\" />
        </div>
        <input class=\"boton\" type=\"submit\" value=\"Consultar\" />
    </form>
    <div class=\"error\">";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "formulario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 14,  44 => 10,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "formulario.html.twig", "D:\\Turno_manana\\XAMPP\\DAW\\php\\ejercicio7_4c\\plantillas\\formulario.html.twig");
    }
}
