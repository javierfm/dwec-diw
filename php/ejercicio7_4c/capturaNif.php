<?php
 /********************************************************************************************
  *  Ejercicio7_4c, curso 2016/17.                                                            *
  * ******************************************************************************************/
   include 'Autoloader.php';
   include 'funcionesDAO.php';

   $diccionario = array('nif' => "", 'error' => "");

   session_start();
   if (isset($_SESSION['nif'])){//Mira si hay nif en session, si lo hay redireccionamos a index.php
      header ("Location: index.php");
   } 

   if (isset($_POST['nif'])){ 
      $nif = strtoupper(trim($_POST['nif'])); 
      $diccionario['nif'] = $nif;
      if ($nif == null) {
          $diccionario['error'] = 'Tien que teclear un NIF';
      } else {
          $alumno = leerAlumno($nif); 
          if ($alumno == null){ 
              $diccionario['error'] = "El alumnu nun ta rexistrau";
          } else
              $notas = leerNotas($nif);
		  //Guarda la información en la session
              if (!empty($notas)){ 
                  $_SESSION['nif'] = $nif;
                  $_SESSION['alumno'] = $alumno;
                  $_SESSION['notas'] = $notas;
                  header ("Location: index.php");
              } else {
                  $diccionario['error'] = 'El alumnu nun tien notes en ningún módulu.';
              }
          }
      }

   Twig_Autoloader::register();
   $loader = new Twig_Loader_Filesystem('plantillas');
   $twig = new Twig_Environment($loader, array(
                          'cache' => 'compiladas','auto_reload' => true));   
   print $twig->render("formulario.html.twig", $diccionario);
?>
