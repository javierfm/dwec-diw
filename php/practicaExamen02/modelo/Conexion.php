<?php

/**
 * Created by PhpStorm.
 * User: Ferja
 * Date: 07/03/2017
 * Time: 9:12
 */
class Conexion
{
    private static $conexion=null;

    //Devuelve un objeto PDO
    public static function obtenerConexion(){
        if(self::$conexion==null){
            $datosConexion=self::leerDatosConexion();
            $opciones = array(PDO::ATTR_PERSISTENT => true);//Solicita una conexión persistente, en vez de crear una nueva conexión
            if(substr_count($datosConexion['dsn'], 'mysql') > 0){
                $opciones[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';//Valores que se reciben desde BBDD en UTF-8
            }
            try{
                self::$conexion=new PDO($datosConexion['dsn'],$datosConexion['usuario'],$datosConexion['password'],$opciones);
                //TODO Si la BBDD es oracle formatear la fecha de salida
            }
            catch (PDOException $ex){
                print "<p>Error: No puede conectarse con la base de datos.</p>\n";
                print "<p>Error: ".$ex->getMessage()."</p>\n";
                die();
            }
        }

        return self::$conexion;
    }


    //Recupera los datos para la conexion a la BBDD
    private static function leerDatosConexion(){
        libxml_use_internal_errors(true);
        if(!$xml = simplexml_load_file('modelo/conexion.xml')){
            print "Error abriendo el fichero con los datos de conexión.<br/>";
            foreach(libxml_get_errors() as $error) {
                echo $error->message.'<br/>';
            }
            die();
        }
        return array("dsn" => $xml->dsn,
            "usuario" => $xml->usuario,
            "password" => $xml->password);
    }
}