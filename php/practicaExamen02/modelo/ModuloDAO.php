<?php

/**
 * Created by PhpStorm.
 * User: Ferja
 * Date: 07/03/2017
 * Time: 17:29
 */
class ModuloDAO
{
    public static function getModulos(){
        $conexion=Conexion::obtenerConexion();
        $consulta="SELECT * FROM aga_modulos ORDER BY NOMBRE";
        $sentencia=$conexion->prepare($consulta);
        $sentencia->execute();
        $listado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        return $listado;
    }

    public static function getAlumnosModulo($cod_modulo){
        $conexion=Conexion::obtenerConexion();
        $consulta="SELECT * FROM aga_alumnos WHERE NIF NOT IN(SELECT ALUMNO FROM aga_nocursados WHERE MODULO=:cod_modulo)";
        $sentencia=$conexion->prepare($consulta);
        $sentencia->bindParam(':cod_modulo',$cod_modulo);
        $sentencia->execute();
        $listado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        return $listado;
    }

    public static function getNombreModulo($cod_modulo){
        $conexion=Conexion::obtenerConexion();
        $consulta="SELECT NOMBRE FROM aga_modulos WHERE CODIGO=:cod_modulo";
        $sentencia=$conexion->prepare($consulta);
        $sentencia->bindParam(':cod_modulo',$cod_modulo);
        $sentencia->execute();
        $listado=$sentencia->fetch(PDO::FETCH_ASSOC);
        var_dump($listado['NOMBRE']);
        return $listado['NOMBRE'];
    }
}