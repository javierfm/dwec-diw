<?php

/* listado.twig */
class __TwigTemplate_36d162afaac3a54e19570976299250aed4edd02de7cd5920c574b8fcfff72355 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Listado de modulos</title>
</head>
<body>
<h1>Lista de alumnos por módulo</h1>
<fieldset>
    <legend>Selección de módulo</legend>
    <ul>
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modulos"]) ? $context["modulos"] : null));
        foreach ($context['_seq'] as $context["clave"] => $context["modulo"]) {
            // line 13
            echo "        <li><a href=\"index.php?modulo=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modulo"], "CODIGO", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modulo"], "NOMBRE", array()), "html", null, true);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['clave'], $context['modulo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    </ul>
</fieldset>
<fieldset>
    <legend>";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["modulo"]) ? $context["modulo"] : null), "html", null, true);
        echo "</legend>

<ul>
    ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["alumnos"]) ? $context["alumnos"] : null));
        foreach ($context['_seq'] as $context["clave"] => $context["alumno"]) {
            // line 22
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["alumno"], "NOMBRE", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["alumno"], "APE1", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["alumno"], "APE2", array()), "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['clave'], $context['alumno'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "</ul>
</fieldset>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "listado.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 24,  62 => 22,  58 => 21,  52 => 18,  47 => 15,  36 => 13,  32 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "listado.twig", "D:\\Turno_manana\\XAMPP\\DAW\\php\\practicaExamen02\\plantillas\\listado.twig");
    }
}
