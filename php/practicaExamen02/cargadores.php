<?php
 /********************************************************************************************
  *  Cargadores Twig y propio.                                                        *
  * ******************************************************************************************/
   include 'Twig/Autoloader.php';
   include 'autocargador.php';	

   Class Cargadores{
      private $twig;
   		public function __construct(){
   			    Twig_Autoloader::register();//Registers Twig_Autoloader as an SPL autoloader.
            //Carga plantillas desde el sistema de archivos. Este cargador puede encontrar plantillas en los directorios del sistema de archivos y es la manera preferida de cargarlas:
            $loader = new Twig_Loader_Filesystem('plantillas');
            $this->twig = new Twig_Environment($loader, array('cache' => 'compiladas','auto_reload' => true));
            Autocargador::registrar();
            Autocargador::setDirectorios("modelo");
   		}

      public function respuesta($plantilla, $datos){
          $plantilla = $this->twig->loadTemplate($plantilla);
          print $plantilla->render($datos);
      }
   }