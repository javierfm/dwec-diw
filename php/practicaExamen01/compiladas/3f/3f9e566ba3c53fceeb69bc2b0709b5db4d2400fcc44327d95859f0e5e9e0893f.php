<?php

/* nueva-cita.html */
class __TwigTemplate_c4307e9a58c255610c9937115fdd4e89c4305c6b3c6211fe188381253e52797e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Nueva cita con ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "nombre", array()), "html", null, true);
        echo "</title></head>
<body>
    <form action=\"grabar-cita.php\" method=\"post\">
        <fieldset>
            <legend>Datos del médico</legend>
            <input type=\"text\" name=\"nombremedico\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "nombre", array()), "html", null, true);
        echo "\" contenteditable=\"false\">
            <input type=\"text\" name=\"ape1medico\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape1", array()), "html", null, true);
        echo "\" contenteditable=\"false\">
            <input type=\"text\" name=\"ape2medico\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "ape2", array()), "html", null, true);
        echo "\" contenteditable=\"false\">
            <input type=\"text\" name=\"nifmedico\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "nif", array()), "html", null, true);
        echo "\" contenteditable=\"false\">
            <input type=\"text\" name=\"especialidadmedico\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["medico"]) ? $context["medico"] : null), "especialidad", array()), "html", null, true);
        echo "\" contenteditable=\"false\">
        </fieldset>

        <fieldset>
            <legend>Datos del paciente</legend>
        <input type=\"text\" name=\"nombre\" placeholder=\"nombre\">
        <input type=\"text\" name=\"ape1\" placeholder=\"apellido 1\">
        <input type=\"text\" name=\"ape2\" placeholder=\"apellido 2\">
        <input type=\"text\" name=\"nifpaciente\" placeholder=\"NIF paciente\">
        <input type=\"text\" name=\"fecha\" placeholder=\"23/02/1977\">
        <input type=\"text\" name=\"hora\" placeholder=\"23:00\">
        </fieldset>
        <input type=\"submit\" value=\"Grabar\">
    </form>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "nueva-cita.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 14,  45 => 13,  41 => 12,  37 => 11,  33 => 10,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "nueva-cita.html", "E:\\Dropbox\\www\\DAW\\PHP\\practicaExamen01\\plantillas\\nueva-cita.html");
    }
}
