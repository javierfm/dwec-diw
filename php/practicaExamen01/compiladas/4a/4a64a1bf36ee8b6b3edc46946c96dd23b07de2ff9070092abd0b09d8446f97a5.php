<?php

/* ver-citas.html */
class __TwigTemplate_841af6c2cbee74276f3b8372b128bd220b764e9768eec5af04d7a1c958e32d37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Ver citas</title>
</head>
<body>
<h1>Citas disponibles</h1>
<ul>
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["citas"]) ? $context["citas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["cita"]) {
            // line 11
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nombrePaciente", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape1Paciente", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "ape2Paciente", array()), "html", null, true);
            echo " <a href=\"eliminar-cita.php?medico=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nifMedico", array()), "html", null, true);
            echo "&paciente=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "nifPaciente", array()), "html", null, true);
            echo "&fecha=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cita"], "getFechaString", array()), "html", null, true);
            echo "\">borrar cita</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cita'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
</ul>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "ver-citas.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 13,  34 => 11,  30 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ver-citas.html", "E:\\Dropbox\\www\\DAW\\PHP\\practicaExamen01\\plantillas\\ver-citas.html");
    }
}
