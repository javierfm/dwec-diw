<?php

/* index.html */
class __TwigTemplate_0ef632f932e7aa41447e3aae697472b63e6c2917b0c660d88ffc55a18360cd8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Pedir citas</title>
</head>
<body>
<form action=\"nueva-cita.php\" method=\"post\">
    <select name=\"medico\">
        ";
        // line 10
        $context["contador"] = 0;
        // line 11
        echo "        ";
        $context["espe"] = "";
        // line 12
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listaMedicos"]) ? $context["listaMedicos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["medico"]) {
            // line 13
            echo "            ";
            if (((isset($context["contador"]) ? $context["contador"] : null) == 0)) {
                // line 14
                echo "                <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "especialidad", array()), "html", null, true);
                echo "\">
            ";
            }
            // line 16
            echo "                    ";
            if ((((isset($context["espe"]) ? $context["espe"] : null) != $this->getAttribute($context["medico"], "especialidad", array())) && ((isset($context["contador"]) ? $context["contador"] : null) > 0))) {
                // line 17
                echo "                </optgroup>
                    <optgroup label=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "especialidad", array()), "html", null, true);
                echo "\">
                        ";
            }
            // line 20
            echo "            <option name=\"medico\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "nif", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "ape1", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["medico"], "ape2", array()), "html", null, true);
            echo "</option>
                    ";
            // line 21
            $context["espe"] = $this->getAttribute($context["medico"], "especialidad", array());
            // line 22
            echo "                    ";
            $context["contador"] = ((isset($context["contador"]) ? $context["contador"] : null) + 1);
            // line 23
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['medico'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "    </select>
    <input type=\"submit\" value=\"nueva cita\">
</form>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 24,  76 => 23,  73 => 22,  71 => 21,  60 => 20,  55 => 18,  52 => 17,  49 => 16,  43 => 14,  40 => 13,  35 => 12,  32 => 11,  30 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html", "E:\\Dropbox\\www\\DAW\\PHP\\practicaExamen01\\plantillas\\index.html");
    }
}
