<?php
include 'cargadores.php';
$cargadores=new Cargadores();
session_start();

//Lo que llega por post
$medico=$_POST['nifmedico'];
$paciente=$_POST['nifpaciente'];
$nombre=$_POST['nombre'];
$ape1=$_POST['ape1'];
$ape2=$_POST['ape2'];
$fecha=$_POST['fecha'];
$hora=$_POST['hora'];

//Creo una nueva cita
$cita=new Cita();
$cita->setNifMedico($medico);
$cita->setNifPaciente($paciente);
$cita->setNombrePaciente($nombre);
$cita->setApe1Paciente($ape1);
$cita->setApe2Paciente($ape2);
//Formato con que se recibe la fecha desde el formulario
$formato = 'd/m/Y H:i:s';
$stringFecha=$fecha." ".$hora.":00";
$fechaCita=DateTime::createFromFormat($formato,$stringFecha);
$cita->setFecha($fechaCita);
//Inserto
$citaDao=new CitaDAO();
if($citaDao->insertarCita($cita)){
    $_SESSION['cita']=$cita;
    header ("Location: ver-citas.php");
}
//TODO controlar pantalla si no inserta con éxito
else{

}


