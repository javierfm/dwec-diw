<?php

include 'cargadores.php';
$cargadores=new Cargadores();
$diccionario=array('listaMedicos' => "", 'error' => "");

/*
El procesador php recupera los datos de session, mira si en la peticion hay una cookie de sesion, si no hay cookie session_start()
crea un id de sesion y se envia en la respuesta. Al cerrar el navegador la cookie se deshecha.
Si hay cookie recupera los objetos en php.ini hay un parámetro que indica donde guarda las sesiones session.save_path
Serializa-Deserializa
*/
session_start();//Si no se pone este método no se puede hacer uso de sesiones


$medicoDAO=new MedicoDAO();
//Listado de medicos
$diccionario['listaMedicos']=$medicoDAO->obtenerMedicos();
//Cargo plantilla
$cargadores->respuesta('index.html',$diccionario);