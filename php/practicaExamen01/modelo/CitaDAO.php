<?php

/**
 * Created by PhpStorm.
 * User: Ferja
 * Date: 07/03/2017
 * Time: 13:37
 */
class CitaDAO
{
    public $cita;

    function __construct()
    {
        $this->cita=new Cita();
    }

    //Inserta una nueva cita devuelve true o false si se inserta con exito
    //CON PDOSTATEMENT Y PRAMETROS
    public function insertarCita($cita){
        $ultimoId=null;
        $this->cita=$cita;
        $parametros=array();
        $parametros['nif_paciente']=$this->cita->getNifPaciente();
        $parametros['nif_medico']=$this->cita->getNifMedico();
        $parametros['fecha']=$this->cita->getFechaString();
        $parametros['nombre']=$this->cita->getNombrePaciente();
        $parametros['ape1']=$this->cita->getApe1Paciente();
        $parametros['ape2']=$this->cita->getApe2Paciente();
        try{
            $sql="INSERT INTO CITAS (NIF_PACIENTE,NIF_MEDICO,FECHA,NOMBRE,APE1,APE2) VALUES (:nif_paciente,:nif_medico,:fecha,:nombre,:ape1,:ape2)";
            $conexionPDO=Conexion::obtenerConexion();
            $statementPDO=$conexionPDO->prepare($sql);
            $exito=$statementPDO->execute($parametros);
        }
        catch (PDOException $exceptionex){
            print_r($exceptionex->getMessage());
        }
        return $exito;
    }

    //Borra una determinada cita
    //PDO STATEMENT CON EXECUTE y PARAMETROS DEVUELVE TRUE O FALSE EN CASO DE ERROR
    public function borrarCita($medico,$paciente,$fecha){

        $sql="DELETE FROM CITAS WHERE NIF_PACIENTE=:nif_paciente AND NIF_MEDICO=:nif_medico AND FECHA=:fecha";
        $conexionPDO=Conexion::obtenerConexion();
        $statementPDO=$conexionPDO->prepare($sql);
        $statementPDO->bindParam(':nif_medico',$medico);
        $statementPDO->bindParam(':nif_paciente',$paciente);
        $statementPDO->bindParam(':fecha',$fecha);

        return $statementPDO->execute();
    }

    //Lista la cita correspondiente a las claves que se le van a pasar
    //CON FECTHALL PARAMETROS Y PDO:ASSOC
    public function listarCitas($cita){
        $listadoCitas=array();
        $nif_medico=$cita->getNifMedico();
        $nif_paciente=$cita->getNifPaciente();
        $sql="SELECT * FROM CITAS WHERE NIF_PACIENTE=:nif_paciente AND NIF_MEDICO=:nif_medico ORDER BY FECHA";
        $conexionPDO=Conexion::obtenerConexion();
        $statementPDO=$conexionPDO->prepare($sql);
        $statementPDO->bindParam(':nif_medico',$nif_medico);
        $statementPDO->bindParam(':nif_paciente',$nif_paciente);
        $statementPDO->setFetchMode(PDO::FETCH_ASSOC);
        $statementPDO->execute();
        $citas=$statementPDO->fetchAll();
        var_dump($citas);

        foreach ($citas as $c){
            $cita=new Cita();
            $cita->setNifMedico($c['NIF_MEDICO']);
            $cita->setNifPaciente($c['NIF_PACIENTE']);
            $cita->setFechaString($c['FECHA']);
            $cita->setNombrePaciente($c['NOMBRE']);
            $cita->setApe1Paciente($c['APE1']);
            $cita->setApe2Paciente($c['APE2']);
            $listadoCitas[]=$cita;
        }

        return $listadoCitas;
    }
}