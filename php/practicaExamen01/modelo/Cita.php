<?php

/**
 * Created by PhpStorm.
 * User: Ferja
 * Date: 07/03/2017
 * Time: 13:35
 */
class Cita
{
    private $nifMedico="";
    private $nifPaciente="";
    private $nombrePaciente="";
    private $ape1Paciente="";
    private $ape2Paciente="";
    private $fecha="";
    private $fechaString="";

    /**
     * @return string
     */
    public function getNifMedico()
    {
        return $this->nifMedico;
    }

    /**
     * @param string $nifMedico
     */
    public function setNifMedico($nifMedico)
    {
        $this->nifMedico = $nifMedico;
    }

    /**
     * @return string
     */
    public function getNifPaciente()
    {
        return $this->nifPaciente;
    }

    /**
     * @param string $nifPaciente
     */
    public function setNifPaciente($nifPaciente)
    {
        $this->nifPaciente = $nifPaciente;
    }

    /**
     * @return string
     */
    public function getNombrePaciente()
    {
        return $this->nombrePaciente;
    }

    /**
     * @param string $nombrePaciente
     */
    public function setNombrePaciente($nombrePaciente)
    {
        $this->nombrePaciente = $nombrePaciente;
    }

    /**
     * @return string
     */
    public function getApe1Paciente()
    {
        return $this->ape1Paciente;
    }

    /**
     * @param string $ape1Paciente
     */
    public function setApe1Paciente($ape1Paciente)
    {
        $this->ape1Paciente = $ape1Paciente;
    }

    /**
     * @return string
     */
    public function getApe2Paciente()
    {
        return $this->ape2Paciente;
    }

    /**
     * @param string $ape2Paciente
     */
    public function setApe2Paciente($ape2Paciente)
    {
        $this->ape2Paciente = $ape2Paciente;
    }

    /**
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function setFechaString($fechaString){
        //Formato con que se recibe la fecha desde  BBDD
        $formato = 'Y-m-d H:i:s';
        $this->setFecha(DateTime::createFromFormat($formato,$fechaString));
    }

    //Devuelve la fecha pero en un formato  YYYY-MM-DD HH:MM:SS
    public function  getFechaString(){
        return date_format($this->getFecha(),"Y-m-d H:i:s");
    }

}