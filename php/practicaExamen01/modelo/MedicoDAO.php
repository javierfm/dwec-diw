<?php

class MedicoDAO
{
    public $medico;
    private $listaMedicos=Array();

    public function __construct(){
        $this->medico=new Medico();
    }

    //Devuelve listado de medicos ordenados por especialidad
    //OBTIENE DESDE BBDD SIN PARAMETROS Y SIN STATMENT, CON UNA QUERY DE LA PDO Y SE OBTIENE UN ARRAY DE OBJETOS MEDICO PDO::FETCH_CLASS
    public function obtenerMedicos(){
        //Se renombran los campos ya que están en mayúsculas en la BBDD y las propiedades en la clase van en minúscula
        $sql="SELECT NIF AS 'nif',NOMBRE AS 'nombre', APE1 AS 'ape1',APE2 AS 'ape2',ESPECIALIDAD AS 'especialidad' FROM MEDICOS ORDER BY ESPECIALIDAD,NOMBRE,APE1";
        var_dump($sql);

        //Representa una conexion entre la base de datos y el PHP
        $conexionPDO=Conexion::obtenerConexion();

        //Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto PDOStatement
        //Especifica que el método de obtención debe devolver una nueva instancia de la clase solicitada, haciendo corresponder las columnas con los nombres de las propiedades de la clase.
        return $statementPDO=$conexionPDO->query($sql)->fetchAll(PDO::FETCH_CLASS,'Medico');

        //$statementPDO=$conexionPDO->prepare($sql);
        //$statementPDO->setFetchMode(PDO::FETCH_CLASS,'Medico');
        //$statementPDO->execute();

        //while ($m=$statementPDO->fetch(PDO::FETCH_CLASS)){
        //    $this->listaMedicos[]=$m;
        //    var_dump($m);
        //}

        //var_dump($this->listaMedicos);
        //return $this->listaMedicos;
    }

    //Devuelve un medico a partir de su NIF
    //OBTIEENE DESDE BBDD CON PARAMETRO Y CON STATMENT  PDO:ASSOCC y FETCH()
    public function obtenerMedico($nif){
        $sql="SELECT NIF AS 'nif',NOMBRE AS 'nombre', APE1 AS 'ape1',APE2 AS 'ape2',ESPECIALIDAD AS 'especialidad' FROM MEDICOS WHERE NIF=:nifMedico ORDER BY ESPECIALIDAD,NOMBRE,APE1";
        $conexionPDO=Conexion::obtenerConexion();
        $statementPDO=$conexionPDO->prepare($sql);
        $statementPDO->bindParam(':nifMedico',$nif);
        $statementPDO->setFetchMode(PDO::FETCH_ASSOC);
        $statementPDO->execute();
        $fila=$statementPDO->fetch();
        $this->medico->setNif($fila['nif']);
        $this->medico->setNombre($fila['nombre']);
        $this->medico->setApe1($fila['ape1']);
        $this->medico->setApe2($fila['ape2']);
        $this->medico->setEspecialidad($fila['especialidad']);
        //$user = $pdo->query('SELECT name FROM users LIMIT 1')->fetchObject('User');

    }
}