<?php

/**
 * Created by PhpStorm.
 * User: Ferja
 * Date: 07/03/2017
 * Time: 9:50
 */
class Medico
{
    private $nif="";
    private $nombre="";
    private $ape1="";
    private $ape2="";
    private $especialidad="";

    /**
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * @param string $nif
     */
    public function setNif($nif)
    {
        $this->nif = $nif;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApe1()
    {
        return $this->ape1;
    }

    /**
     * @param string $ape1
     */
    public function setApe1($ape1)
    {
        $this->ape1 = $ape1;
    }

    /**
     * @return string
     */
    public function getApe2()
    {
        return $this->ape2;
    }

    /**
     * @param string $ape2
     */
    public function setApe2($ape2)
    {
        $this->ape2 = $ape2;
    }

    /**
     * @return string
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }

    /**
     * @param string $especialidad
     */
    public function setEspecialidad($especialidad)
    {
        $this->especialidad = $especialidad;
    }


}