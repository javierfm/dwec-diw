<!DOCTYPE html>
<html lang="es">
    <head>
        <title>La web 2.0</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
    <div id="navegacion"><a href="../index.html">Volver a inicio</a></div>
        <header>
            <h1>Ejercicios XHTML <!-- fin cabecera -->
        </h1>
        </header><section>

        <section>
            <h2>Listado de ejercicios </h2>
            <ul>
			<li><a href="c1-ejercicio-01.html">Capítulo 1: Ejercicio 1</a></li>
			<li><a href="c1-ejercicio-02.html">Capítulo 2: Ejercicio 2</a></li>
            <li><a href="ejercicio-01.html">Libros Web: Ejercicio 1</a></li>
            <li><a href="ejercicio-02.html">Libros Web: Ejercicio 2</a></li>
            <li><a href="ejercicio-03.html">Libros Web: Ejercicio 3</a> </li>
            <li><a href="ejercicio-04.html">Libros Web: Ejercicio 4 </a></li>
            <li><a href="ejercicio-05.html">Libros Web: Ejercicio 5 </a></li>
            <li><a href="ejercicio-06/indice.html">Libros Web: Ejercicio 6 &amp; 7</a></li>
            <li><a href="ejercicio-08.htm">Libros Web: Ejercicio 8 </a></li>
            <li><a href="ejercicio-09.htm">Libros Web: Ejercicio 9 </a></li>
            <li><a href="ejercicio-10/indice.html">Libros Web: Ejercicio 10 </a></li>
            <li><a href="ejercicio-11.htm">Libros Web: Ejercicio11</a></li>
            <li><a href="ejercicio-12.htm">Libros Web: Ejercicio12</a></li>
            <li><a href="ejercicio-13.htm">Libros Web: Ejercicio13</a></li>
            <li><a href="ejercicio-14.htm">Libros Web: Ejercicio14</a></li>
            <li><a href="ejercicio-15.htm">Libros Web: Ejercicio15</a></li>
            <li><a href="ejercicio-16.htm">Libros Web: Ejercicio16</a></li>
            </ul>
        </section> <!--fin seccion contenido principal -->
        <footer>
            2016 2º DAW
        </footer><!-- fin pie de página -->
    </body>
</html>
