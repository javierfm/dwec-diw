//Guardo Array de enlaces en una variable global para que las distintas funciones tengan acceso a estas
var enlaces=document.getElementsByTagName("a");

//Escribe por pantalla el número de enlaces que tiene
function writeNumeroEnlaces(){
    document.write("Nº de enlaces en la página: "+ enlaces.length+"<br>");
}

//Escribe en pàntalla la dirección a la que apunta el penúltimo enlaces
function writeUrlUltimoEnlace(){
    var penultimoEnlace=enlaces[(enlaces.length)-2];
    document.write("URL a la que apunta el penúltimo enlace: "+penultimoEnlace.attributes[0].value+"<br>");
}

//Escribe en pantalla el número de enlaces que apuntan a "prueba"
function writeEnlacesPrueba(){
    var numLinks=0;
    //recorro los enlaces y aquellos cuyo atributo sea http://prueba lo añado
    for (var i=0;i<enlaces.length;i++){
        console.log(enlaces[i].getAttributeNode("href").value);
        if(enlaces[i].getAttributeNode("href").value=="http://prueba"){
            numLinks++;
        }
    }

    document.write("El número de enlaces que apuntan a prueba son: "+numLinks+"<br>");
}

//Escribe pòr pantalla el número de enlaces del tercer párrafo
function writeLinksTercerParrafo(){
    var ps=document.getElementsByTagName("p");
    var enlaces=ps[2].getElementsByTagName("a").length;
    
    document.write("El número de enlaces del segundo párrafo son: "+enlaces+"<br>");
}


