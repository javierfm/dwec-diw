//Array con posibles letras de NIF
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

//Le pido al usuario su número de DNI
var numeroDni=prompt("Introduce tu DNI");

//Le pido al usuario la letra de su DNI
var letraDni=prompt("Introduzca la letra de su DNI");

//Compruebo que el número es correcto calculo si la letra es válida
if(numeroDni>0 && numeroDni<=99999999){
	var letraAux=letras[numeroDni%23];

	if(letraDni==letraAux){
		alert("La letra introducida es correcta");
	}
	else{
		alert("Las letras no se corresponden");
	}
}
else{
	alert("El DNI introducido no es correcto");
}