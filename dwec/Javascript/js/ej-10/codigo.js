function esPalin(cadena) {
    //Elimino espacios en blanco
    var cadenaArray =cadena.replace(/ /g,"");

    //paso todos los caracteres a minúsculas
    cadenaArray=cadenaArray.toLowerCase();

    //convierto el string a Array
    cadenaArray=cadenaArray.split("");

    //Creo un Array a partir del que tenemos
    var cadenaArrayInversa=cadenaArray.slice(0);

    //Invierto el orden del Array
    cadenaArrayInversa=cadenaArrayInversa.reverse();

    var p = true;
    var i;
    for (i=0;i<cadenaArray.length;i++) {
        if (cadenaArray[i] != cadenaArrayInversa[i]) {
            p = false;
            break;
        }
    }

    if (p) {
        alert(cadena+" es Palindroma");
    }
    else{
    	alert(cadena+" NO Palindroma");
    }
}
