//Declaro Array
var valores = [true, 5, false, "hola", "adios", 2];

//1. Determino que elemento de texto es mayor
var aux = "";
if (valores[3] > valores[4]) {
	aux = " es mayor que ";
} else {
	aux = "es menor que ";
}
alert(valores[3] + aux + valores[4]);

/*2. Utilizando exclusivamente los dos valores booleanos del array,
 determinar los operadores necesarios para obtener un resultado true y otro resultado false*/
alert(valores[0] === !valores[2]);
alert(valores[0] === valores[2]);

//3. Determinar el resultado de las cinco operaciones matemáticas realizadas con los dos elementos numéricos
alert("suma: " + (valores[1] + valores[5]));
alert("resta: " + (valores[1] - valores[5]));
alert("producto: " + (valores[1] * valores[5]));
alert("división: " + (valores[1] / valores[5]));
alert("módulo: " + (valores[1] % valores[5]));

