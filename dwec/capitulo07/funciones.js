/*Script que permite realizar peticiones usando Ajax*/
function darAjax() {
	var ajax = false;
	try {
		ajax = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch (e) {
		try {
			ajax = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (E) {
			if (!ajax && typeof XMLHttpRequest != 'undefined') ajax = new XMLHttpRequest();
		}
	}
	return ajax;
}
//Realiza peticion al servidor
function validarUsuario(usuario, clave) {
	var ajax = darAjax();
	var mensaje = "Inválido";
	//realiza peticion al server con el script PHP que hemos creado
	ajax.open("GET", "valida.php?usuario=" + usuario + "&clave=" + clave, false);
	ajax.onreadystatechange = function () {
		if (ajax.readyState == 4) mensaje = ajax.responseText;
	}
	ajax.send(null);
	alert("El usuario es: " + mensaje);
	return null;
}