<?php require_once('Connections/libreria.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_RecordsetAutores = 5;
$pageNum_RecordsetAutores = 0;
if (isset($_GET['pageNum_RecordsetAutores'])) {
  $pageNum_RecordsetAutores = $_GET['pageNum_RecordsetAutores'];
}
$startRow_RecordsetAutores = $pageNum_RecordsetAutores * $maxRows_RecordsetAutores;

mysql_select_db($database_libreria, $libreria);
$query_RecordsetAutores = "SELECT * FROM lib_autor ORDER BY lib_autor.cod_autor";
$query_limit_RecordsetAutores = sprintf("%s LIMIT %d, %d", $query_RecordsetAutores, $startRow_RecordsetAutores, $maxRows_RecordsetAutores);
$RecordsetAutores = mysql_query($query_limit_RecordsetAutores, $libreria) or die(mysql_error());
$row_RecordsetAutores = mysql_fetch_assoc($RecordsetAutores);

if (isset($_GET['totalRows_RecordsetAutores'])) {
  $totalRows_RecordsetAutores = $_GET['totalRows_RecordsetAutores'];
} else {
  $all_RecordsetAutores = mysql_query($query_RecordsetAutores);
  $totalRows_RecordsetAutores = mysql_num_rows($all_RecordsetAutores);
}
$totalPages_RecordsetAutores = ceil($totalRows_RecordsetAutores/$maxRows_RecordsetAutores)-1;

$queryString_RecordsetAutores = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_RecordsetAutores") == false && 
        stristr($param, "totalRows_RecordsetAutores") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_RecordsetAutores = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_RecordsetAutores = sprintf("&totalRows_RecordsetAutores=%d%s", $totalRows_RecordsetAutores, $queryString_RecordsetAutores);
?>
<!DOCTYPE HTML>
<html lang="es">
<head>
<meta charset="utf-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<title>Listar autores</title>
</head>
<body>
<div class="container">
<ul>
    <li><a href="../../../index.html">Javier Fernández</a></li>
    <li><a href="../index.html">Desarrollo web en entorno cliente (DWEC)</a></li>
    <li class="activo">Gestión de librería: Autores</li>
  </ul>
<div class="row">
<h1>Listar autores</h1>
<h2>Buscar un registro por su nombre</h2>
<form action="resultado-busqueda-autor.php" method="post" enctype="application/x-www-form-urlencoded" name="form1">
<input name="nombre" type="text" placeholder="Buscar autor...">
<button class="btn btn-default" type="submit">Buscar!</button>
</form>
<h2>Insertar un nuevo registro</h2>
<p>Haga click en el <a href="insertar-autor.php">enlace</a> para añadir un nuevo autor.</p>
<table class="table table-striped">
<tr>
<td>Id</td>
<td>Nombre del autor</td>
<td>Acciones</td>
</tr>
<?php do { ?>
<tr>
  <td><?php echo $row_RecordsetAutores['cod_autor']; ?></td>
  <td><?php echo $row_RecordsetAutores['nombre']; ?></td>
  <td><a href="borrar-autor-accion.php?cod_autor=<?php echo $row_RecordsetAutores['cod_autor']; ?>">Borrar</a> | <a href="modificar-autor-accion.php?cod_autor=<?php echo $row_RecordsetAutores['cod_autor']; ?>">Modificar</a></td>
</tr>
<?php } while ($row_RecordsetAutores = mysql_fetch_assoc($RecordsetAutores)); ?>
</table>
</div>
<nav aria-label="...">
  <ul class="pager">
  <li class="previous"><a href="<?php printf("%s?pageNum_RecordsetAutores=%d%s", $currentPage, 0, $queryString_RecordsetAutores); ?>"><< Más antiguo</a></li>
    <li><a href="<?php printf("%s?pageNum_RecordsetAutores=%d%s", $currentPage, max(0, $pageNum_RecordsetAutores - 1), $queryString_RecordsetAutores); ?>">Anterior</a></li>
    <li><a href="<?php printf("%s?pageNum_RecordsetAutores=%d%s", $currentPage, min($totalPages_RecordsetAutores, $pageNum_RecordsetAutores + 1), $queryString_RecordsetAutores); ?>">Siguiente</a></li>
	<li class="next"><a href="<?php printf("%s?pageNum_RecordsetAutores=%d%s", $currentPage, $totalPages_RecordsetAutores, $queryString_RecordsetAutores); ?>">Más nuevo >></a></li>
  </ul>
</nav>

  
</div>
</div>
</body>
</html>
<?php
mysql_free_result($RecordsetAutores);
?>
