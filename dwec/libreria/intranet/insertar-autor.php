<?php require_once('Connections/libreria.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO lib_autor (cod_autor, nombre) VALUES (%s, %s)",
                       GetSQLValueString($_POST['cod_autor'], "int"),
                       GetSQLValueString($_POST['nombre'], "text"));

  mysql_select_db($database_libreria, $libreria);
  $Result1 = mysql_query($insertSQL, $libreria) or die(mysql_error());

  $insertGoTo = "listar-autores.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form2")) {
  $insertSQL = sprintf("INSERT INTO lib_autor (nombre) VALUES (%s)",
                       GetSQLValueString($_POST['nombre'], "text"));

  mysql_select_db($database_libreria, $libreria);
  $Result1 = mysql_query($insertSQL, $libreria) or die(mysql_error());

  $insertGoTo = "listar-autores.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!DOCTYPE HTML>
<html lang="es">
<head>
<meta charset="utf-8">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<title>Listar autores</title>
</head>
<body>
<div class="container">
<ul>
    <li><a href="../../../index.html">Javier Fernández</a></li>
    <li><a href="../index.html">Desarrollo web en entorno cliente (DWEC)</a></li>
    <li class="activo">Gestión de librería: Autores</li>
  </ul>
<div class="row">
<h1>Insertar autor</h1>
  <form method="post" name="form2" action="<?php echo $editFormAction; ?>" class="form-inline">
    <table>
      <tr valign="baseline">
        <td nowrap align="right">Nombre:</td>
        <td><input type="text" name="nombre" value="" size="32"></td>
		<td><input type="submit" value="Insertar registro" class="btn btn-default"></td>
      </tr>
      <tr valign="baseline">
      </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form2">
  </form>
</div>
</div>
</div>
</body>
</html>
