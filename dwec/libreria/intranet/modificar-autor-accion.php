<?php require_once('Connections/libreria.php'); ?>

$cod_autor=$_GET["cod_autor"];
echo $cod_autor;
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE lib_autor SET nombre=%s WHERE cod_autor=%s",
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($cod_autor, "int"));
					   echo $updateSQL;

  mysql_select_db($database_libreria, $libreria);
  $Result1 = mysql_query($updateSQL, $libreria) or die(mysql_error());

  $updateGoTo = "borrado-exito.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

mysql_select_db($database_libreria, $libreria);
$query_RecordsetRegistros = "SELECT * FROM lib_autor WHERE cod_autor='$cod_autor'";
$RecordsetRegistros = mysql_query($query_RecordsetRegistros, $libreria) or die(mysql_error());
$row_RecordsetRegistros = mysql_fetch_assoc($RecordsetRegistros);
$totalRows_RecordsetRegistros = mysql_num_rows($RecordsetRegistros);
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Modificar registro</title>
</head>
<ul>
    <li><a href="../../../index.html">Javier Fernández</a></li>
    <li><a href="../index.html">Desarrollo web en entorno cliente (DWEC)</a></li>
    <li class="activo">Gestión de librería: Autores</li>
  </ul>
<body>
<h1>Modificar registro</h1>


<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right">Nombre:</td>
      <td><input type="text" name="nombre" value="<?php echo $row_RecordsetRegistros['nombre']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Actualizar registro"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="cod_autor" value="<?php $cod_autor; ?>">
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($RecordsetRegistros);
?>
