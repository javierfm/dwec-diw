<?php
session_start();

?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Acceso al documento desde código</title>
    <link rel="stylesheet" type="text/css" href="../../css/styles.css"> </head>
<body>
   <div id="cabecera">
    <a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
</div>
    <div id="navegacion">
        <ul>
            <li><a href="/miWebVirtual/">Javier Fernández</a></li>
            <li><a href="../index.html"><acronym title="Desarrollo Web en entorno cliente">DWEC</acronym></a></li>
            <li class="activo">Ejemplo de uso de sesiones en <em>PHP</em></li>
        </ul>
        <div style="clear: left"></div>
    </div>
    <div id="contenedor">
        <?php
if(isset($_SESSION['logeado'])&&$_SESSION['logeado']==true){
	
	
	
	
	echo "<h1>Bienvenido ".$_SESSION['login']."</h1>";
	echo "<p>Este es tu mensaje: ".$_SESSION['mensaje']."</p>";
	
	
	
	
}




else{
	
	
	
	
	echo "<h1>Esta pagina es solo para usuarios logeados.</h1>";
	
	
	
	
	echo "Ir a <a href='login.php'>Login</a>";
	
	
	
	
}




?>
    </div>
</body>
</html>
</body>