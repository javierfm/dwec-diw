<?php
session_start();
?>
    <html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Acceso al documento desde código</title>
        <link rel="stylesheet" type="text/css" href="../../css/styles.css"> </head>

    <body>
        <div id="cabecera">
            <a href="http://fleming.informatica-fleming.com/"><img src="../../img/logo_fleming.jpg" alt="IES Doctor Fleming" width="338" height="111" /></a>
        </div>
        <div id="navegacion">
            <ul>
                <li><a href="/miWebVirtual/">Javier Fernández</a></li>
                <li><a href="../index.html"><acronym title="Desarrollo Web en entorno cliente">DWEC</acronym></a></li>
                <li class="activo">Ejemplo de uso de sesiones en <em>PHP</em></li>
            </ul>
            <div style="clear: left"></div>
        </div>
        <div id="contenedor">
            <form action="check-login.php" method="post">
                <label>Nombre Usuario:</label>
                <br>
                <input name="username" type="text" id="username" required placeholder="alumnoDAW">
                <br>
                <br>
                <label>Password:</label>
                <br>
                <input name="password" type="password" id="password" required placeholder="alumnoDAW">
                <br>
                <br>
                <label>Mensaje:</label>
                <br>
                <input name="mensaje" type="text" id="mensaje" required placeholder="escribe aquí lo que quieras">
                <br>
                <br>
                <input type="submit" name="Submit" value="LOGIN"> </div>
    </body>

    </html>
    </body>