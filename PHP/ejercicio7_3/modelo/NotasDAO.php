<?php
require_once 'Conexion.php';

class NotasDAO
{
    public function getNota($alumno,$codModulo){
        $nota=null;
        $conexion=Conexion::obtenerConexion();
        $query_params=["nif_alumno"=>$alumno,"cod_modulo"=>$codModulo];
        $statement=$conexion->prepare("SELECT NOTA,FECHA FROM AGA_NOTAS WHERE NIF_ALU=:nif_alumno AND COD_MODULO=:cod_modulo");
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
		$conexion=null;
        return $resultado;
    }
}