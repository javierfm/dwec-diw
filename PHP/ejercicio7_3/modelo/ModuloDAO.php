<?php
require_once 'Conexion.php';

class ModuloDAO{
    public function getModulos(){
        $listadoModulos=[];
        $conexion=Conexion::obtenerConexion();
        $sql='SELECT * FROM AGA_MODULOS ORDER BY NOMBRE';
        foreach ($conexion->query($sql) as $row){
            $listadoModulos[$row['CODIGO']]=$row['NOMBRE'];
        }
        $conexion=null;//se cierra conexion
		return $listadoModulos;
    }
	
	public function getNombreModulo($codigoModulo){
		$nombreModulo=null;
		$conexion=Conexion::obtenerConexion();
		$query_params=["codigo_modulo"=>$codigoModulo];
        $statement=$conexion->prepare("SELECT NOMBRE FROM AGA_MODULOS WHERE CODIGO=:codigo_modulo");
        $statement->execute($query_params);
        $nombreModulo=$statement->fetchColumn();
		$conexion=null;
		return $nombreModulo;
	}
}