<?php

/**
 * Clase modelo del ejercicio
 *
 * @author Ferja
 */
class Notas {

    //variable estatica, es compartida por todas las instancias de la clase
    private static $notas = array("11111111A" => 5,
        "22222222B" => 7,
        "33333333C" => 8,
        "44444444D" => 10);

    //Devuelve la nota de un alumno ($nif) recibido como parámetro. Devuelve la nota o msg de error.
    public function leerNota($nif) {
        //al ser variable estática se hace referencia con self:: no con $this->
        if (isset(self::$notas[$nif])) {
            return self::$notas[$nif];
        }
        else{
            "El alumno con el NIF ".$nif." no está matriculado";
        }
    }

}
?>