<?php
require_once 'Conexion.php';

class AlumnoDAO
{
    public function getNombreAlumno($nif){
        $nombreAlumno=null;
        $conexion=Conexion::obtenerConexion();
        $query_params=["nif_alumno"=>$nif];
        $statement=$conexion->prepare("SELECT NOMBRE,APE1,APE2 FROM aga_alumnos WHERE NIF=:nif_alumno");
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
        $nombreAlumno=$resultado[0]['NOMBRE']." ".$resultado[0]['APE1']." ".$resultado[0]['APE2'];
        $conexion=null;
        return $nombreAlumno;
    }
}