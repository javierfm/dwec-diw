<?php
require_once 'Conexion.php';

class NotasDAO
{
    public function getNotas($nif,$codModulo){
        var_dump($nif,$codModulo);
        $conexion=Conexion::obtenerConexion();
        $query_params=['nif_alumno'=>$nif,'cod_modulo'=>$codModulo];
        $consulta="SELECT NOTA,FECHA FROM aga_notas WHERE NIF_ALU=:nif_alumno AND COD_MODULO=:cod_modulo";
        $statement=$conexion->prepare($consulta);
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
        $conexion=null;
        return $resultado;
    }
}