<?php
require_once 'Conexion.php';

class ModuloDAO{
    public function getModulos(){
        $listadoModulos=[];
        $conexion=Conexion::obtenerConexion();
        $sql='SELECT * FROM aga_modulos';
        foreach ($conexion->query($sql) as $row){
            $listadoModulos[$row['CODIGO']]=$row['NOMBRE'];
        }
        $conexion=null;//se cierra conexion
		return $listadoModulos;
    }
	
	public function getNombreModulo($codigoModulo){
		$nombreModulo=null;
		$conexion=Conexion::obtenerConexion();
		$query_params=["codigo_modulo"=>$codigoModulo];
        $statement=$conexion->prepare("SELECT NOMBRE FROM aga_modulos WHERE CODIGO=:codigo_modulo");
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
        $nombreModulo=$resultado[0]['NOMBRE'];
		$conexion=null;
		return $nombreModulo;
	}

	public function getModulosAlumno($nif){
        $conexion=Conexion::obtenerConexion();
        $query_params=["nif"=>$nif];
        $consulta="SELECT DISTINCT aga_notas.COD_MODULO,aga_modulos.NOMBRE FROM aga_notas ";
        $consulta.="JOIN aga_modulos ON aga_notas.COD_MODULO=aga_modulos.CODIGO ";
        $consulta.="WHERE aga_notas.NIF_ALU=:nif";
        $statement=$conexion->prepare($consulta);
        $statement->execute($query_params);
        $resultado=$statement->fetchAll(PDO::FETCH_ASSOC);
        $conexion=null;//se cierra conexion
        return $resultado;
    }
}