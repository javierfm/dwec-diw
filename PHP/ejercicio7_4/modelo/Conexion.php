<?php
//require_once 'dbconfig.php';

class Conexion{
    public static function obtenerConexion(){
        $xml = simplexml_load_file('modelo/datos-acceso.xml');
        try{
            return new PDO($xml->dsn,$xml->user,$xml->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8',PDO::ATTR_TIMEOUT => 30));
        }
        catch (PDOException $ex){
            $ex->getMessage();
        }
    }
}