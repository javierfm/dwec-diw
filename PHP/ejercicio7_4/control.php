<?php
require 'modelo/ModuloDAO.php';
require 'modelo/NotasDAO.php';
require 'modelo/AlumnoDAO.php';
require 'vista/Salida.php';
?>
	<?php

abstract class Index {
	//Diccionario que recibe valores del formulario. Por defecto, con la primera carga de la página están vacíos
    private static $valoresForm = array('error' => "", 'modulos' => "",'cod_modulo'=>"",'calificaciones'=>"",'nif'=>"",'alumno'=>"",'nombre_modulo'=>'');
	
	//Método que se lanza cuando se carga la página
    static function run()
    {

        $modulosDAO = new ModuloDAO();
        $notasDAO = new NotasDAO();
        $alumnoDAO=new AlumnoDAO();

        //Hay NIF
        if (isset($_POST['nif']))
        {
            //NIF null
            if ($_POST['nif'] == NULL)
            {
                //Nif vacio, cambiamos parámetros
                self::$valoresForm['error']="Nif vacío";
            }
            //NIF not null
            else
            {
                $nif = $_POST['nif'];
                //Módulos en los que existen calificaciones para NIF seleccionado
                $listaModulos = $modulosDAO->getModulosAlumno($nif);
                //Hay módulos para el alumno
                if (count($listaModulos) > 0) {
					//Si no llega ningun modulo seleccionado por POST se toma el primero
					if(isset($_POST['modulo_seleccionado'])){
						$modulo_seleccionado=$_POST['modulo_seleccionado'];
						 self::$valoresForm['modulo_seleccionado']=$modulo_seleccionado;
						 self::$valoresForm['calificaciones']=$notasDAO->getNotas($nif,$modulo_seleccionado);
						self::$valoresForm['nombre_modulo']=$modulosDAO->getNombreModulo($modulo_seleccionado);
					}
					else{
						//Array de Arrays, primer elemento
						$modulo_seleccionado=$listaModulos[0];
						//Ese primer elemtno (Array) tomar el valor de COD_MODULO
						self::$valoresForm['modulo_seleccionado']=$modulo_seleccionado['COD_MODULO'];
						self::$valoresForm['calificaciones']=$notasDAO->getNotas($nif,$modulo_seleccionado['COD_MODULO']);
						self::$valoresForm['nombre_modulo']=$modulo_seleccionado['NOMBRE'];
					}
                    
					self::$valoresForm['nif']=$nif;
                    self::$valoresForm['modulos']=$listaModulos;
                    //Se puede mejorar y controlar si existe el valor por POST y sino hacer la consulta
                    self::$valoresForm['alumno']=$alumnoDAO->getNombreAlumno($nif);
                }

                //TODO controlar si el alumno no tiene ninguna nota (no tiene modulos)
                else {
					self::$valoresForm['error']="El alumno no tiene notas";
                }
            }
        }
        //TODO controlar cuando no Hay NIF
        else
        {
			self::$valoresForm['error']="Debe introducir un NIF";
        }
        //Llamamos a la vista para que saque por pantalla
        Salida::mostrarVista(self::$valoresForm);
    }
}
Index::run();
?>