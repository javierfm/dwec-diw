<?php

/* formulario.html */
class __TwigTemplate_e498636d53643a17683fc7fbd74e9795e5cd84971d3a8562449aa350f66f6159 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head lang=\"es\">
\t<meta charset=\"utf-8\">
\t<title>Consulta de notas</title>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"estilo.css\" /> </head>

<body>
\t<header> <img src=\"imagenes/logo_fleming.jpg\" id=\"logo\" />
\t\t<div id=\"daw\">Desenvolvimientu d'aplicaciones web</div>
\t</header>
\t<h1>Consulta de notes</h1> ";
        // line 13
        if (( !(null === (isset($context["error"]) ? $context["error"] : null)) &&  !twig_test_empty((isset($context["error"]) ? $context["error"] : null)))) {
            // line 14
            echo "\t<div class=\"error\"> ";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo " </div> <a href=\"index.html\">Hacer otra consulta</a> ";
        } else {
            // line 15
            echo "\t<div id=\"central\">
\t\t<h2>";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["alumno"]) ? $context["alumno"] : null), "html", null, true);
            echo "</h2>
\t\t<h3>";
            // line 17
            echo twig_escape_filter($this->env, (isset($context["nombre_modulo"]) ? $context["nombre_modulo"] : null), "html", null, true);
            echo "</h3>
\t\t<form action=\"control.php\" method=\"post\"> ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modulos"]) ? $context["modulos"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 19
                echo "\t\t\t<input type=\"radio\" name=\"modulo_seleccionado\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "COD_MODULO", array()), "html", null, true);
                echo "\" ";
                if (((isset($context["modulo_seleccionado"]) ? $context["modulo_seleccionado"] : null) == $this->getAttribute($context["value"], "COD_MODULO", array()))) {
                    echo "checked ";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "NOMBRE", array()), "html", null, true);
                echo "
\t\t\t<br> ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "\t\t\t<input type=\"hidden\" name=\"nif\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["nif"]) ? $context["nif"] : null), "html", null, true);
            echo "\">
\t\t\t<input class=\"boton\" type=\"submit\" value=\"Consultar\" /> </form>
\t\t<div class=\"linea\">
\t\t\t<div class=\"resultado\"> ";
            // line 24
            if (( !(null === (isset($context["calificaciones"]) ? $context["calificaciones"] : null)) &&  !twig_test_empty((isset($context["calificaciones"]) ? $context["calificaciones"] : null)))) {
                // line 25
                echo "\t\t\t\t<table>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Nota</th>
\t\t\t\t\t\t<th>Fecha nota</th>
\t\t\t\t\t</tr> ";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["calificaciones"]) ? $context["calificaciones"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 30
                    echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>";
                    // line 31
                    echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "NOTA", array()), "html", null, true);
                    echo "</td>
\t\t\t\t\t\t<td>";
                    // line 32
                    echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "FECHA", array()), "html", null, true);
                    echo "</td>
\t\t\t\t\t</tr> ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 33
                echo " </table>";
            }
            echo " </div>
\t\t</div>
\t</div> ";
        }
        // line 35
        echo " </body>

</html>";
    }

    public function getTemplateName()
    {
        return "formulario.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 35,  104 => 33,  96 => 32,  92 => 31,  89 => 30,  85 => 29,  79 => 25,  77 => 24,  70 => 21,  55 => 19,  51 => 18,  47 => 17,  43 => 16,  40 => 15,  35 => 14,  33 => 13,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "formulario.html", "D:\\Turno_manana\\XAMPP\\DAW\\php\\ejercicio7_4\\formulario.html");
    }
}
