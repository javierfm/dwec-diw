<?php

function getNif() {
    return trim($_GET["nif"]);
}

function getNotas() {
    return array(
        "9426830" => array(
            "Desarrollo aplicaciones en servidor" => 1,
            "Desarrollo aplicaciones cliente" => 2,
            "Diseño de interfaces web" => 3,
            "Despliegue de aplicaciones web" => 4
        ),
        "10421102" => array(
            "Desarrollo aplicaciones en servidor" => 4,
            "Desarrollo aplicaciones cliente" => 3,
            "Diseño de interfaces web" => 2,
            "Despliegue de aplicaciones web" => 1
        )
    );
}
?>
<?php

function devolverNotas() {
    $nif= getNif();
    $notasAlumnos = getNotas();
    $cadenaSalida = "";
    //Formulario vacío
    if (strlen($nif) == 0) {
        $cadenaSalida .= "<h1>El nif no puede estar vacío</h1>";
    } //Formulario con datos
    else {
        //Existen notas para ese alumno
        if (isset($notasAlumnos[$nif])) {
            $notasAlumno = $notasAlumnos[$nif];
            $cadenaSalida = "<h1>Notas para el alumno con nif $nif</h1>";
            $cadenaSalida .= "<table><tr><th>Módulo</th><th>Nota</th></tr>";
            foreach ($notasAlumno as $modulo => $nota) {
                $cadenaSalida .= "<tr><td>$modulo</td><td>$nota</td>";
            }
            $cadenaSalida .= "</table>";
        }
        //No existe alumno matriculado
        else {
            $cadenaSalida = "<h1>El alumno no está registrado en el centro</h1>";
        }
    }
    $cadenaSalida .= "<p><a href=\"index.html\">Volver a consultar</a></p>";
    return $cadenaSalida;
}
?>