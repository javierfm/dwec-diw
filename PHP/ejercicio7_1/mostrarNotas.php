<?php
//Array con las notas sobre los que voy a consultar
global $notasAlumnos;
 $notasAlumnos = array(
    "9426830" => array(
        "Desarrollo aplicaciones en servidor" => 1,
        "Desarrollo aplicaciones cliente" => 2,
        "Diseño de interfaces web" => 3,
        "Despliegue de aplicaciones web" => 4
    ),
    "10421102" => array(
        "Desarrollo aplicaciones en servidor" => 4,
        "Desarrollo aplicaciones cliente" => 3,
        "Diseño de interfaces web" => 2,
        "Despliegue de aplicaciones web" => 1
    )
);
//Obtengo el parámetro que llega del form
 global $nif;
$nif = trim($_POST["nif"]);
//var_dump($nif);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Consultar nota</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <?php
            //Formulario vacío
            if (strlen($nif) == 0) {
                echo "<h1>El nif no puede estar vacío</h1>";
            } //Formulario con datos
            else {
                //Existen notas para ese alumno
                if (isset($notasAlumnos[$nif])) {
                    $notasAlumno = $notasAlumnos[$nif];
                    $salida="<h1>Notas para el alumno con nif $nif</h1>";
                    $salida.="<table><tr><th>Módulo</th><th>Nota</th></tr>";
                    foreach ($notasAlumno as $modulo => $nota) {
                        $salida.="<tr><td>$modulo</td><td>$nota</td>";
                    }
                    $salida.="</table>";
                    echo $salida;
                }
                //No existe alumno matriculado
                else {
                    echo "<h1>El alumno no está registrado en el centro</h1>";
                }
            }
            echo "<p><a href=\"index.html\">Volver a consultar</a></p>";
            ?>
        </div>


    </body>
</html>
